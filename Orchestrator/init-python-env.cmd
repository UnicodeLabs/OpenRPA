chcp 65001
@echo off
echo Формат использования init-python-env.cmd [имя запускаемого процесса.exe] [имя убиваемого процесса.exe]
echo Пример использования init-python-env.cmd orpa-rbt.exe orpa-rbt.exe

if [%1]==[] goto :python-env
goto create-exe
:create-exe
copy /Y "%~dp0..\Resources\WPy64-3720\python-3.7.2.amd64\python.exe" "%~dp0..\Resources\WPy64-3720\python-3.7.2.amd64\%1"
if [%2]==[] goto :python-env
goto taskkill
:taskkill
taskkill /im "%2" /F /fi "username eq %username%"
goto :python-env
:python-env
set CD_PREV=%cd%
cd /d "%~dp0..\Resources\WPy64-3720\python-3.7.2.amd64"
set PATH=%cd%;%cd%\Scripts;%PATH%
cd /d "%~dp0..\Sources"
set PYTHONPATH=%cd%;%PYTHONPATH%
cd %CD_PREV%
:eof
echo Инициализация Python окружения прошла успешно!
@echo on