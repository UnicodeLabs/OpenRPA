import time
time.sleep(1)
splitter1 = 'Рабочий процесс'
splitter2 = 'Пример доступных данных'
splitter3 = 'Пример запуска роботов'
title = 'КАДРОВИК'

introHeader = 'Применение:'
introText = 'Робот-кадровик помогает проводить отбор кандидатов'

workflowHeader1 = 'Шаг 1: Размещение вакансии'
workflowDescription1 = 'Выполняет человек'
workflowHint1 = 'Размещение вакансии в telegram/HH.ru'
workflowHeader2 = 'Шаг 2: Тестирование'
workflowDescription2 = 'Выполняет робот'
workflowHint2 = 'Автоматическое тестирования кандидатов'
workflowHeader3 = 'Шаг 3: Подведение итогов'
workflowDescription3 = 'Выполняет робот'
workflowHint3 = 'Автоматическое подведение итогов тестирования'
workflowHeader4 = 'Шаг 4: Личное собеседование'
workflowDescription4 = 'Выполняет человек'
workflowHint4 = 'Собеседование с лучшими кандидатами'
workflowHeader5 = 'Шаг 5: Оформление будущего сотрудника'
workflowDescription5 = 'Выполняет человек'
workflowHint5 = 'Подписание трудового договора'


questionHeaderMsg = 'Вопросы'
questionDescriptionMsg = 'Список вопросов для первого этапа'
resultHeaderMsg = 'Результаты'
resultDescriptionMsg = 'Сводка по всем рекрутам'
logHeaderMsg = 'Лог'
logDescriptionMsg = 'Лог за текущие сутки'

dropdownInit = 'Выберите робота...'
dropdownRobot1 = 'Робот на вакансию 1'
dropdownRobot2 = 'Робот на вакансию 2'








html = f'''<!doctype html>
<html lang="en">


<head>
    <title> Test </title>
    <link rel="stylesheet" href="/HR_01/styles">
</head>


<body>


<div class="card" style="width:450px;" id="card1">
        <div class="content">
                       <center><h1>{title}</h1></center><br>
                        <div class="ui message">
                            <div class="header">{introHeader}</div>
                            <p>{introText}</p>
                        </div>

                        <h4><span>{splitter1}</span></h4>

                        <div class="ui relaxed divided list">
                          <div class="item">
                            <i class="user icon"></i>
                            <div class="content">
                              <div class="header" data-tooltip="{workflowHint1}">{workflowHeader1}</div>
                              <div class="description">{workflowDescription1}</div>
                            </div>
                          </div>
                          <div class="item">
                            <i class="reddit alien icon"></i>
                            <div class="content">
                                <div class="header" data-tooltip="{workflowHint2}">{workflowHeader2}</div>
                              <div class="description">{workflowDescription2}</div>
                            </div>
                          </div>
                          <div class="item">
                            <i class="reddit alien icon"></i>
                            <div class="content">
                              <div class="header" data-tooltip="{workflowHint3}">{workflowHeader3}</div>
                              <div class="description">{workflowDescription3}</div>
                            </div>
                          </div>
                            <div class="item">
                            <i class="user icon"></i>
                            <div class="content">
                              <div class="header" data-tooltip="{workflowHint4}">{workflowHeader4}</div>
                              <div class="description">{workflowDescription4}</div>
                            </div>
                          </div>
                            <div class="item">
                            <i class="user icon"></i>
                            <div class="content">
                              <div class="header" data-tooltip="{workflowHint5}">{workflowHeader5}</div>
                              <div class="description">{workflowDescription5}</div>
                            </div>
                          </div>
                        </div>

                        <h4><span>{splitter2}</span></h4>

                        <div class="ui list">
                          <div class="item">
                            <i class="file icon"></i>
                            <div class="content">
                              <a class="header" onclick="get_data('question')">{questionHeaderMsg}</a>
                              <div class="description">{questionDescriptionMsg}</div>
                            </div>
                          </div>
                          <div class="item">
                            <i class="adress book icon"></i>
                            <div class="content">
                              <a class="header" onclick="get_data('results')">{resultHeaderMsg}</a>
                              <div class="description">{resultDescriptionMsg}</div>
                            </div>
                          </div>
                          <div class="item">
                            <i class="file alternate icon"></i>
                            <div class="content">
                              <a class="header" onclick="get_data('logs')">{logHeaderMsg}</a>
                              <div class="description">{logDescriptionMsg}</div>
                            </div>
                          </div>
                        </div>

                        <h4><span>{splitter3}</span></h4>

                            <div class="ui list">
                              <div class="item">
                                <i class="telegram icon"></i>
                                <div id="Vacancy1Status" class="content"></div>
                              </div>
                              <div class="item">
                                <i class="telegram icon"></i>
                                <div id="Vacancy2Status" class="content"></div>
                              </div>
                            </div>


                            <div class="ui selection dropdown" onchange="bot_change()">
                                <input type="hidden" name="bot">
                                  <i class="dropdown icon"></i>
                                  <div class="default text">{dropdownInit}</div>
                                  <div class="menu">
                                    <div class="item" data-value="1">{dropdownRobot1}</div>
                                    <div class="item" data-value="0">{dropdownRobot2}</div>
                                  </div>
                            </div>

                            <p><div id="conteiner"></div></p>



        </div>
    </div>


<script src="/HR_01/scripts"></script>
<script type="text/javascript" src="https://viewer.diagrams.net/js/viewer-static.min.js"></script>
</body>


</html>'''

from pyOpenRPA.Tools import CrossOS
from pyOpenRPA import Orchestrator  # Import orchestrator main
from pyOpenRPA.Orchestrator.Server import app 
import threading
from fastapi import Depends
from fastapi.responses import PlainTextResponse
from fastapi.responses import FileResponse

# Подключени файлов связанных с роботом-кадровиком01
@app.get(path="/HR_01/questions",tags=["HR_01"])
def get_file():
    return FileResponse(CrossOS.PathStr("Demo\\HR_01\\Questions.csv"))
    
@app.get(path="/HR_01/results",tags=["HR_01"])
def get_file():
    return FileResponse(CrossOS.PathStr("Demo\\HR_01\\All_results.csv"))
    
@app.get(path="/HR_01/logs",tags=["HR_01"])
def get_file():
    return FileResponse(CrossOS.PathStr("Demo\\HR_01\\log.txt"))

@app.get(path="/HR_01/scripts",tags=["HR_01"])
def get_file():
    return FileResponse(CrossOS.PathStr("Demo\\HR_01\\HR_officer01.js"))

@app.get(path="/HR_01/styles",tags=["HR_01"])
def get_file():
    return FileResponse(CrossOS.PathStr("Demo\\HR_01\\HR_officer01.css"))

lCPManager = Orchestrator.Managers.ControlPanel(inControlPanelNameStr="HR_officer01ControlPanel",
        inRefreshHTMLJinja2TemplatePathStr=CrossOS.PathStr("Demo\\HR_01\\index.html"), inJinja2TemplateRefreshBool = True)


with open(CrossOS.PathStr("Demo\\HR_01\\index.html"), 'w', encoding='UTF-8') as file:
    file.write(html)
