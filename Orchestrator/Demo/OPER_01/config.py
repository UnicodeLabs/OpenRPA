
from pyOpenRPA import Orchestrator
from pyOpenRPA.Orchestrator.Managers import ControlPanel
import time
import os
import sys
import socket
import datetime
import urllib.parse
time.sleep(1.5)
g_cp_name_str = "OPER_00"
g_repo_name_str = os.path.abspath(__file__).split("\\")[-5]
g_repo_package_cp_path_str = os.path.abspath("\\".join(os.path.abspath(__file__).split("\\")[:-1]))

# User settings
g_host_str = socket.gethostname().upper() # Identify PC

g_control_panel = ControlPanel(inControlPanelNameStr=g_cp_name_str,
    inRefreshHTMLJinja2TemplatePathStr=os.path.join(g_repo_package_cp_path_str, "html_jinja2.xhtml"),
    inJinja2TemplateRefreshBool=True,inRobotNameStr=g_repo_name_str)
g_jinja_context_dict = {"settings": sys.modules[__name__], 
    "urllib_parse_quote_plus": urllib.parse.quote_plus, "g_host_str": g_host_str, "g_repo_name_str": g_repo_name_str}
g_control_panel.Jinja2DataUpdateDictSet(inJinja2DataUpdateDict=g_jinja_context_dict)
