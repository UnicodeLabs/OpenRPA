import time
time.sleep(1)


splitter1 = 'Рабочий процесс'
splitter2 = 'Окно настройки робота'

title = 'КАЗНАЧЕЙ'

introHeader = 'Применение:'
introText = 'Робот-казначей выполняет выгрузку банковских выписок'

workflowHeader1 = 'Шаг 1: Авторизация в клиент-банке'
workflowDescription1 = 'Выполняет робот'
workflowHint1 = 'Авторизация на сайте банка'
workflowHeader2 = 'Шаг 2: Выгрузка выписок'
workflowDescription2 = 'Выполняет робот'
workflowHint2 = 'Скачивание выписок с сайта банка'
workflowHeader3 = 'Шаг 3: Перенаправление выписок'
workflowDescription3 = 'Выполняет робот'
workflowHint3 = 'Например, размещение в сетевой папке'

dropBankTitle = 'Выберите банк...'
dropBank1 = 'Сбербанк'
dropBank2 = 'ВТБ'
dropBank3 = 'Альфа'

dropBankAccountTitle = 'Выберите счет'
dropBankAccount1 = 'Счет 1'
dropBankAccount2 = 'Счет 2'
dropBankAccount3 = 'Счет 3'
dropBankAccountAll = 'Счет XY'

checkboxPerTitle = 'Период:'
checkboxPer1 = 'За день'
checkboxPer2 = 'За месяц'

checkboxFormTitle = ''
checkboxForm1 = 'PDF'
checkboxForm2 = '1C'
checkboxForm3 = 'WORD'
checkboxForm4 = 'EXCEL'

emailTitle = 'Укажите почту (необязательно):'
emailWindow = 'example@mail.ru'

btnDownload = 'Скачать'
btnEmail = 'Отправить'


html = f'''<!doctype html>
<html lang="en">


<head>
    <title> Treasurer </title>
</head>


<body>


<div class="card" style="width:450px;" id="card1">
    <div class="content">
         <center><h1>{title}</h1></center><br>
            <div class="ui message">
                <div class="header">{introHeader}</div>
                <p>{introText}</p>
            </div>

         <h4><span>{splitter1}</span></h4>

        <div class="ui relaxed divided list">
          <div class="item">
            <i class="reddit alien icon"></i>
            <div class="content">
              <div class="header" data-tooltip="{workflowHint1}">{workflowHeader1}</div>
              <div class="description">{workflowDescription1}</div>
            </div>
          </div>
          <div class="item">
            <i class="reddit alien icon"></i>
            <div class="content">
              <div class="header" data-tooltip="{workflowHint2}">{workflowHeader2}</div>
              <div class="description">{workflowDescription2}</div>
            </div>
          </div>
          <div class="item">
            <i class="reddit alien icon"></i>
            <div class="content">
              <div class="header" data-tooltip="{workflowHint3}">{workflowHeader3}</div>
              <div class="description">{workflowDescription3}</div>
            </div>
          </div>
        </div>

        <h4><span>{splitter2}</span></h4>

        <div class="ui selection dropdown">
          <input type="hidden" name="bank">
          <i class="dropdown icon"></i>
          <div class="default text">{dropBankTitle}</div>
          <div class="menu">
            <div class="item" data-value="2">{dropBank1}</div>
            <div class="item" data-value="1">{dropBank2}</div>
            <div class="item" data-value="0">{dropBank3}</div>
          </div>
        </div><br><br>

        <select class="ui fluid search dropdown" style="font-family: 'Lato', 'Helvetica Neue', Arial, Helvetica, sans-serif" multiple="">
          <option value="">{dropBankAccountTitle}</option>
          <option value="All">{dropBankAccountAll}</option>
          <option value="Ch1">{dropBankAccount1}</option>
          <option value="Ch2">{dropBankAccount2}</option>
          <option value="Ch3">{dropBankAccount3}</option>
        </select><br>

        <div class="ui form">
          <div class="inline fields">
            <label>{checkboxPerTitle}</label>
            <div class="field">
              <div class="ui radio checkbox">
                <input type="radio" name="time">
                <label>{checkboxPer1}</label>
              </div>
            </div>
            <div class="field">
              <div class="ui radio checkbox">
                <input type="radio" name="time">
                <label>{checkboxPer2}</label>
              </div>
            </div>
          </div>
        </div>


        <div class="ui form">
          <div class="inline fields">
            <label>{checkboxFormTitle}</label>
            <div class="field">
              <div class="ui radio checkbox">
                <input type="radio" name="format">
                <label>{checkboxForm1}</label>
              </div>
            </div>
            <div class="field">
              <div class="ui radio checkbox">
                <input type="radio" name="format">
                <label>{checkboxForm2}</label>
              </div>
            </div>
              <div class="field">
              <div class="ui radio checkbox">
                <input type="radio" name="format">
                <label>{checkboxForm3}</label>
              </div>
            </div>
              <div class="field">
              <div class="ui radio checkbox">
                <input type="radio" name="format">
                <label>{checkboxForm4}</label>
              </div>
            </div>
          </div>
        </div>


       <div><b>{emailTitle}</b></div>
        <p><div class="ui input focus">
          <input type="text" placeholder="{emailWindow}">
        </div></p>

        <button class="ui right labeled icon button"><i class="download icon"></i> {btnDownload} </button>
        <button class="ui right labeled icon button"><i class="envelope icon"></i> {btnEmail} </button>


    </div>
</div>


<script src="/TREASURER_01/scripts"></script>
</body>


</html>'''

from pyOpenRPA.Tools import CrossOS
from pyOpenRPA import Orchestrator  # Import orchestrator main
from pyOpenRPA.Orchestrator.Server import app 
import threading
from fastapi import Depends
from fastapi.responses import PlainTextResponse
from fastapi.responses import FileResponse

# Подключени файлов связанных с роботом-казначеем01
@app.get(path="/TREASURER_01/scripts",tags=["TREASURER_01"])
def get_file():
    return FileResponse("Demo\\TREASURER_01\\Treasurer01.js")



lCPManager = Orchestrator.Managers.ControlPanel(inControlPanelNameStr="Treasurer01ControlPanel",
        inRefreshHTMLJinja2TemplatePathStr=CrossOS.PathStr("Demo\\TREASURER_01\\index.html"), inJinja2TemplateRefreshBool = True)


with open("Demo\\TREASURER_01\\index.html", 'w', encoding='UTF-8') as file:
    file.write(html)
