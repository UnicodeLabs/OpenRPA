init-python-env.cmd
pip uninstall jupyter-core
pip uninstall jupyter-contrib-core
pip install "..\..\Resources\PyPackages\jupyter_core-4.6.3-py2.py3-none-any.whl"
pip install "..\..\Resources\PyPackages\jupyter_contrib_core-0.3.3-py2.py3-none-any.whl"
jupyter contrib nbextension install --user
jupyter nbextension enable Hinterland
jupyter nbextension enable highlighter