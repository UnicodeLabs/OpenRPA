LEGEND: 
ORC - ORCHESTRATOR
STD - STUDIO
RBT - ROBOT
AGT - AGENT

[1.3.1]
- ОРКЕСТРАТОР
- - минорные правки в дизайн
- - Orchestrator.OrchestratorPySearchInit - добавлена возможность импорта пакетов с импользованием relative imports внутри
- - Web: Восстановлена функциональность кнопки "Перезагрузить оркестратор"
- - Исправлена ошибка обратной совместимости функций Orchestrator.WebConnect...
- СТУДИЯ
- - UI переведен на русский язык
- - обновлен дизайн по аналогии с порталом и Оркестратором
- - новое наименование процесса "orpa-std.exe"
- РОБОТ
- - Появился модуль захвата звука с микрофонов и с приложений (pyOpenRPA.Robot.Audio)
- ОБЩЕЕ
- - Jupyter: запуск из других дисков, отличных от C://
- - Utils: Функции подготовки файлов / директорий
- - Utils: Text - SimilarityNoCase - Сверка двух строк на схождение (коэффициент от 0.0 до 1.0)

[1.3.0]
- ПОРТИРОВАНО НА LINUX (Ubuntu, Debian, Astra), адаптация функций
- ОРКЕСТРАТОР
- - Manager.ControlPanel: Поддерживает установку пути по формата Linux и Windows
- - Обновление дизайна в соответствии с порталом (шапка + подвал)
- - новое наименование процесса "orpa-orc.exe"
- СТУДИЯ
- - Обновление дизайна в соответствии с порталом (шапка + подвал)
- ДОПОЛНИТЕЛЬНО
- - Jinja2: Создание класса быстрой инициализации pyopenRPA.Tools.Template
- - Клавиша Alt фиксирует координаты мыши при поиске объекта, что позволяет поработать с определением объекта не при наведенной мыши

[1.2.13]
- ДОКУМЕНТАЦИЯ - переведена на русский язык
- ORCHESTRATOR
- - def WebRequestResponseSend(inResponeStr, inRequest=None, inContentTypeStr: str = None, inHeadersDict: dict = None):
- ROBOT
- - Keyboard, add hotkey codes (up, down, ctr, alt, etc)
- - Keyboard, consolidated defs
- - Keyboard, В каждую функцию добавил обработку пост ожидания, если это необходимо для стабилизации робота
- - Mouse, Дополнил примеры в описание каждой функции
- - Mouse, В каждую функцию добавил обработку пост ожидания, если это необходимо для стабилизации робота
- - UIDesktop, Переведен и дополнен


[1.2.12]
2022_Q2
- ORCHESTRATOR
- - WebURL... Support inUseCacheBool - cache the web pages
- - Web server auto detect MIME type from the file name
- - Raise debug session from production. Support init_debug file in working directory
- - You can change the index page (def WebURLIndexChange)
- - Web server add EqualNoParam
- - Add support MIME WOFF2
- - MANAGERS
- - - ControlPanel
- - - Git CI
- - - Process
- - Support Linux Ubuntu
- ROBOT
- - Support Linux Ubuntu
- STUDIO
- - Support Linux Ubuntu

[1.2.0]
!Orchestrator!
- Consolidated processor from old orchestrator and from RDPActive processor (one threaded). Look in GSettings
- - Support old orchestrator structure Processor.
- - - Create BackwardCompatibility def to update structure from old to new processor
- - Support orchestrator start
- - Support scheduler
- - Support old items
- Backward compatibility from 1.1.13
- Add ArgLogger key in Processor struct item
- Old function will be tranformated in Orchestrator defs (from pyOpenRPA.Orchestrator import Orchestrator):
- - def OSCredentialsVerify(inUserStr, inPasswordStr, inDomainStr=""): ## Verify credentials in windows
- - def OSCMD(inCMDStr): ## OS send command in shell locally
- - def OrchestratorRestart(inGSettings=None): ## Orchestrator restart
- - def OrchestratorSessionSave(inGSettings=None): ## Orchestrator session save
- - def GSettingsKeyListValueSet(inGSettings, inValue, inKeyList=[]): # Set value in GSettings by the key list
- - def GSettingsKeyListValueAppend(inGSettings, inValue, inKeyList=[]): # Append value in GSettings by the key list
- - def GSettingsKeyListValueOperatorPlus(inGSettings, inValue, inKeyList=[]): # Operator plus value in GSettings by the key list
- - def GSettingsKeyListValueGet(inGSettings, inKeyList=[]): # Get the value from the GSettings by the key list
- - def ProcessIsStarted(inProcessNameWOExeStr): # Check if process is started
- - def ProcessStart(inPathStr, inArgList, inStopProcessNameWOExeStr=None): # Start process locally [optional: if task name is not started]
- - def ProcessStop(inProcessNameWOExeStr, inCloseForceBool, inUserNameStr = "%username%"): # Stop process
- - def PythonStart(inModulePathStr, inDefNameStr, inArgList=[], inArgDict={}, inLogger = None): # Python import module and start def
- - Add pyOpenRPA.Orchestrator.Core module technical defs
- - - def IsProcessorThread() return True or False
- Orchestrator WEB fix: Don't request screenshot if no modal is active in front
- Add Version field in Orchestrator back + front
- Add front autorefresh if front/back pyOpenRPA versions are differs (see CP_VersionCheck.py) 
- Add absolute/relative import for the control panels
- Add new Orchestrator defs:
- - def UACUpdate(inGSettings, inADLoginStr, inADStr="", inADIsDefaultBool=True, inURLList=[], inCPAllowKeyList=[]): - Update user access
- - def UACSuperTokenUpdate(inGSettings, inSuperTokenStr): # Add supertoken for the all access (it is need for the robot communication without human)
- Create Web HTML / JS generators [pyOpenRPA.Orchestrator.Web.Basic]
- - def JSEscapeForHTMLInline(inJSStr): # Escape JS to the safe JS for the inline JS in HTML tags ATTENTION! Use it only if want to paste JS into HTML tag - not in <script>
- - def HTMLLinkURL(inURLStr, inTitleStr=None, inColorStr=None): # Generate HTML code of the simple URL link by the URL
- - def HTMLLinkJSOnClick(inJSOnClickStr, inTitleStr, inColorStr=None): # Generate HTML code of the simple URL link by the JS when onclick
- Add URL "/pyOpenRPA/ServerData" - consolidated function - in test
- Create new support - CP def can return {"CPKeyStr":{"HTMLStr":"", DataDict:{}}}
- Create CP 2 HTML generator for the backward compatibility
- Orchestrator WEB GUI update - Administrator mode - add log view - in progress
- - Add Server def /pyOpenRPA/ServerLogs
- - Create logger handler for the Client DumpLog
- Create /pyOpenRPA/ActivityListExecute
- Create /pyOpenRPA/Processor 
- Orchestrator.OSCMD Listen output and send to logger 
- Orchestrator.OSCMD Add 2 input args inLogger + inRunAsyncBool 
- WEB Update CMD Input line (tranfer to Log view). Change /Utils/Processor to /pyOpenRPA/ActivityListExecute
- Defs has been added in ProcessorAliasDict as Alias with own def name
- WEB Remove client freeze when back is 
- Create new pyOpenRPA UAC Client hierarchy SettingsTemplate.__UACClientAdminCreate__ - need to update functionallity
- Orchestrator WEB: Update WEB to the new UACClient 
- Create pyOpenRPA.Agent - just prototype, need test
- Create Agent support in Orchestrator (/pyOpenRPA/Agent/O2A and /pyOpenRPA/Agent/A2O)
- Orch: /pyOpenRPA/ServerData - add sub dict "AgentDict" 
- Orch WEB: Create Agent render
- Orch web: Fix eror in serverDataRender when error render
- Agent: Add CMD to kill agent because it is in background mode
- Orch: Add handler to set connection when Agent listen orch (/pyOpenRPA/Agent/O2A)
- Orch start processor Dict in own thread (Processor.ProcessorRunSync(inGSettings))
- Agent: Create Processor in Agent similarly to Orchestrator (pyOpenRPA.Agent.Processor == pyOpenRPA.Orchestrator.Processor)
- Agent: Add Agent defs as Alias in ProcessorDict
- Agent Add 2 defs:
- - def OSFileTextDataStrCreate(inFilePathStr, inFileDataStr, inEncodingStr = "utf-8",inGSettings = None): # Create text file by the string
- - def OSFileBinaryDataBase64StrCreate(inFilePathStr, inFileDataBase64Str,inGSettings = None): # Create binary file by the base64 string (safe for JSON transmition)
- - def OSCMD(inCMDStr, inRunAsyncBool=True, inGSettings = None): # Send CMD to OS. Result return to log + Orchestrator by the A2O connection
- Orc: Add Agent Defs
- - def AgentActivityItemAdd(inGSettings, inHostNameStr, inUserStr, inActivityItemDict): # Add activity in AgentDict
- - def AgentOSCMD(inGSettings, inHostNameStr, inUserStr, inCMDStr): # Send to agent activity item to OSCMD
- - def AgentOSFileBinaryDataBytesCreate(inGSettings, inHostNameStr, inUserStr, inFilePathStr, inFileDataBytes): # Send binary file to Agent (Bytes)
- - def AgentOSFileBinaryDataBase64StrCreate(inGSettings, inHostNameStr, inUserStr, inFilePathStr, inFileDataBase64Str): # Send binary file to Agent (base64 string)
- - def AgentOSFileTextDataStrCreate(inGSettings, inHostNameStr, inUserStr, inFilePathStr, inFileDataStr, inEncodingStr = "utf-8"):  # Send text file to Agent (string)
- Orc WEB: Create mGlobal.pyOpenRPA.ActivityListExecute({}) to test some activities from the front
- Orc - add log about send activity to agent
- Orc RoleHierarchy - support RDP, Support Agent + buttons
- Orc new structure for CP: "CPDict": { # "CPKey": {"HTMLRenderDef":None, "JSONGeneratorDef":None, "JSInitGeneratorDef":None}},
- - Back: inGSettings["CPDict"][RobotKeyStr]["HTMLRenderDef"] > Front: mGlobal.pyOpenRPA.ServerDataDict.CPDict.RobotKeyStr.HTMLStr
- - Back: inGSettings["CPDict"][RobotKeyStr]["JSONGeneratorDef"] > Front: mGlobal.pyOpenRPA.ServerDataDict.CPDict.RobotKeyStr.JSONDict
- - CPDict > HTMLRenderDef > def (inGSettings); def (inRequest, inGSettings); def ()
- - CPDict > JSONGeneratorDef > def (inGSettings); def (inRequest, inGSettings); def ()
- - CPDict > JSInitGeneratorDef > def (inGSettings); def (inRequest, inGSettings); def ()
 Orc connect JSONGenerators to WEB Front (mGlobal.)
- Orc back: add new block: Web
- - def WebCPUpdate(inGSettings, inCPKeyStr, inHTMLRenderDef=None, inJSONGeneratorDef=None, inJSInitGeneratorDef=None): # Add control panel HTML, JSON generator or JS when page init
- Add Working Dir (CWD) + Orchestrator Version Str in ServerData > UserDict
- Orc Web JS - lite refactoring
- def UACKeyListCheck(inRequest, inRoleKeyList): #Check is client is has access for the key list
- def WebUserInfoGet(inRequest): # Return User info about request Return {"DomainUpperStr":"", "UserNameUpperStr": ""}
- def WebUserUACHierarchyGet(inRequest): # Return User UAC Hierarchy DICT Return {...}
- Scheduler
- Refactoring in gSettings (Scheduler > SchedulerDict)
- def SchedulerActivityTimeAddWeekly(inGSettings, inTimeHHMMStr="23:55:", inWeekdayList=[], inActivityList=[]): # Add activity in time weekly
- Scheduler now listen SchedulerDict

- def ProcessorActivityItemAppend(inGSettings, inDef, inArgList=[], inArgDict={}, inArgGSettingsStr=None, inArgLoggerStr=None): # Add Activity item in Processor list
! Scheduler period activity was suppressed ("TimeHH:MMStart" in lItem and "TimeHH:MMStop")
- WEB Connect defs
- def WebURLConnectDef(inGSettings, inMethodStr, inURLStr, inMatchTypeStr, inDef, inContentTypeStr="application/octet-stream"): # Connect URL to DEF
- def WebURLConnectFolder(inGSettings, inMethodStr, inURLStr, inMatchTypeStr, inFolderPathStr): # Connect URL to Folder
- def WebURLConnectFile(inGSettings, inMethodStr, inURLStr, inMatchTypeStr, inFilePathStr, inContentTypeStr="application/octet-stream"): # Connect URL to File

- def RDPTemplateCreate(inLoginStr, inPasswordStr, inHostStr="127.0.0.1", inPortInt = 3389, inWidthPXInt = 1680,  inHeightPXInt = 1050, inUseBothMonitorBool = False, inDepthBitInt = 32, inSharedDriveList=["c"]): # Create some RDP template dict to use it when connect/reconnect
- Update def RDPSessionConnect(inGSettings, inRDPSessionKeyStr, inRDPTemplateDict=None, inHostStr=None, inPortStr=None, inLoginStr=None, inPasswordStr=None): # Create new RDPSession in RobotRDPActive. Attention - activity will be ignored if key is exists 
- Update def RDPSessionReconnect(inGSettings, inRDPSessionKeyStr, inRDPTemplateDict=None): # RDP Session reconnect

- Add alg which find dublicates in RDPList if connection lost was appeared - important to catch 2+ RDP to one RDP configs

- def WebUserIsSuperToken(inRequest, inGSettings): # Return bool if request is authentificated with supetoken (token which is never expires)

- def ProcessorAliasDefCreate(inGSettings, inDef, inAliasStr=None): # Create alias for def (can be used in ActivityItem in field Def)
- WEB
    /// Add ActivityList in processor queue
    mGlobal.pyOpenRPA.ProcessorQueueAdd=function(inActivityList) {
    
    /// Execute ActivityList
    mGlobal.pyOpenRPA.ActivityListExecute=function(inActivityList) {
    
- ORC Defs
def JSActivityListExecute(inActivityList): # Create JS for execute activity list/ activity permanent # USAGE: Orchestrator.Web.Basic.JSActivityListExecute(inActivityList)
def JSProcessorActivityListAdd(inActivityList): # Create JS for send activity list/ activity to the processor # USAGE: Orchestrator.Web.Basic.JSProcessorActivityListAdd(inActivityList)
def ProcessorActivityItemCreate(inDef, inArgList=None, inArgDict=None, inArgGSettingsStr=None, inArgLoggerStr=None): # Create ActivityItem # return dict
def ProcessorAliasDefUpdate(inGSettings=inGSettings, inDef=inDef, inAliasStr=lDefAliasStr)

# Create HTMLLink by the def, argdict, arglist, gsettingsStr, logger Str titleStr, color, (execute permanently)
def HTMLLinkDefExecute(inGSettings, inDef, inArgDict=None, inArgList=None, inArgGSettingsStr="", inArgLoggerStr="", inLinkTitleStr=None, inLinkColorStr=""):

# Create HTMLLink by the def, argdict, arglist, gsettingsStr, logger Str titleStr, color, (add in processor queue)
def HTMLLinkDefProcessor(inGSettings, inDef, inArgDict=None, inArgList=None, inArgGSettingsStr="", inArgLoggerStr="", inLinkTitleStr=None, inLinkColorStr=""):
[1.1.0]
After 2 month test prefinal with new improovements (+RobotRDPActive in Orchestrator + Easy ControlPanelTemplate)
Beta before 1.1.0 (new way of OpenRPA with improvements. Sorry, but no backward compatibility)/ Backward compatibility will start from 1.0.1
[1.0.37]
Minor fix in RobotRDPActive
[1.0.33]
Menu changes - look git
[1.0.31]
Orchestrator new engine - test 2 is ready. Go to PIP
[1.0.30]
RobotScreenActive - robot, which monitor the active screen and run Console session if screen disappear
[1.0.29]
RobotRDPActive minor Fix in str conv
[1.0.28]
RobotRDPActive first version is ready!
[1.0.26]
Robot UIDesktop bug fix in Safe other process function
[1.0.25]
*Dont upload to PyPi* - Not tested
Created safe call function in UIDesktop
UIOSelector_SafeOtherGet_Process

Safe call in UIDesktop for:
- UIOSelectorUIOActivity_Run_Dict
- UIOSelector_Exist_Bool
- UIOSelector_Highlight
- UIOSelector_FocusHighlight
- UIOSelector_SearchChildByMouse_UIOTree
- UIOSelector_GetChildList_UIOList
- UIOSelector_Get_UIOInfoList
- UIOSelector_Get_UIOInfo
- UIOSelector_Get_UIOActivityList

UIOSelectorSecs_WaitAppear_Bool
UIOSelectorSecs_WaitDisappear_Bool
UIOSelectorsSecs_WaitAppear_List
UIOSelectorsSecs_WaitDisappear_List
[1.0.24]
1.0.1 Beta
Refactoring (Studio Orchestrator in pyOpenRPA package)
[1.0.22]
1.0.1 Beta
[1.0.19]
MinorFix in pyOpenRPA.Core
[1.0.18]
MinorFix in pyOpenRPA.Core