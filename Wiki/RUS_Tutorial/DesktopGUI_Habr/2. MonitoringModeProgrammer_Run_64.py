from pyOpenRPA.Robot import UIDesktop # Импорт модуля, который умеет управлять UI элеметами GUI приложений
import time # Библиотека сна алгоритма
import os # Билбиотека системных функций, позволит открять калькулятор, если это потребуется
lUIOSelectorCalculator = [{"title":"Калькулятор","class_name":"CalcFrame","backend":"win32"}] # Сформировали UIO селектор из студии pyOpenRPA
while True: # Вечный цикл
    lExistBool = UIDesktop.UIOSelector_Exist_Bool(inUIOSelector=lUIOSelectorCalculator) # Проверить наличие окна по UIO селектору
    if not lExistBool: # Проверить наличие окна калькулятора
        os.system("calc") # Открыть калькулятор
    else: # Проверить, что окно калькулятора не свернуто
        lUIOCalculator = UIDesktop.UIOSelector_Get_UIO(inSpecificationList=lUIOSelectorCalculator) # Получить UIO экземпляр
        if lUIOCalculator.is_minimized(): # Проверить, что калькулятор находится в свернутом виде
            lUIOCalculator.restore() # Восстановить окно калькулятора из свернутого вида
        else:
            lCalcHex_IsExistBool = UIDesktop.UIOSelector_Exist_Bool(inUIOSelector=[{"class_name":"CalcFrame","backend":"win32"},{ "title":"Hex", "depth_start":3, "depth_end": 3}]) # Проверить наличие UI элемента по UIO селектору
            if not lCalcHex_IsExistBool: # Проверить, что UI элемент отсутствует
                lUIOCalculator.menu_select("&Вид -> &Программист") # Выполнить смену режима калькулятора
    time.sleep(1) # Выполнить сон на 1 сек., после чего перейти на следующую итерацию