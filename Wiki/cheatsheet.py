# Local app interaction
import time
from pyOpenRPA.Robot import UIDesktop

# 1C Start
lDemoBaseSelector = [{"title":"Запуск 1С:Предприятия","class_name":"V8TopLevelFrameTaxiStarter","backend":"uia"},{"title":"DEMO", "depth_start": 5, "depth_end": 5}]
lDemoBase = UIDesktop.UIOSelector_Get_UIO(lDemoBaseSelector)
lDemoBase.draw_outline()
time.sleep(2.0)
lRunBaseSelector = [{"title":"Запуск 1С:Предприятия","class_name":"V8TopLevelFrameTaxiStarter","backend":"uia"},{"title":"1С:Предприятие", "depth_start": 4, "depth_end": 4}]
lRunBase = UIDesktop.UIOSelector_Get_UIO(lRunBaseSelector)
lRunBase.draw_outline()
time.sleep(2.0)
lRunBase.click_input()

# 1C Open order (заказы покупателей)
lOrderNumberSelector = [{"title":"Управление нашей фирмой, редакция 1.6","class_name":"V8TopLevelFrameSDI","backend":"uia"},{"title":"АСФР-000036 Номер", "depth_start": 13, "depth_end": 13}]
UIDesktop.UIOSelector_Get_UIO(lOrderNumberSelector).draw_outline()
UIDesktop.UIOSelector_Get_UIO(lOrderNumberSelector).double_click_input()

time.sleep(1.0)
lCommentSelector = [{"title":"Управление нашей фирмой, редакция 1.6","class_name":"V8TopLevelFrameSDI","backend":"uia"},{"control_type":"Pane"},{"control_type":"Pane"},{"control_type":"Pane"},{"control_type":"Pane"},{"control_type":"Pane"},{"control_type":"Pane"},{"control_type":"Pane"},{"title":"","control_type":"Edit", "depth_start": 1, "depth_end": 10}]
UIDesktop.UIOSelector_Get_UIO(lCommentSelector).draw_outline()
UIDesktop.UIOSelector_Get_UIO(lCommentSelector).set_edit_text("Заказ исполнен роботом")

# Web app interaction
# WIKI TO DO

# Keyboard interaction
import ctypes  # An included library with Python install.
from pyOpenRPA.Robot import Keyboard
from pyOpenRPA.Robot import Clipboard
Keyboard.send("win+r")
time.sleep(0.3)
Keyboard.write("cmd")
time.sleep(0.3)
Keyboard.send("enter")
time.sleep(0.6)
Keyboard.write("echo %time%")
time.sleep(0.3)
Keyboard.send("enter")
time.sleep(0.3)
Keyboard.send("ctrl+a")
time.sleep(0.6)
Clipboard.ClipboardSet("")
Keyboard.send("ctrl+c")
time.sleep(0.6)
lTextRaw = Clipboard.ClipboardGet()
lTimeStr = lTextRaw.split("\n")[-3]

def msg_box(title, text, style):
    return ctypes.windll.user32.MessageBoxW(0, text, title, style)
msg_box('Робот на клавиатуре', f'Робот извлек время из консоли: {lTimeStr}', 0)

# Mouse interaction
from pyOpenRPA.Robot import Mouse
# Letter Я
x = -50
y = 150
Mouse.mouseDown(x+100,y+0)
Mouse.moveTo(x+100,y+100)
Mouse.moveTo(x+100,y+50)
Mouse.moveTo(x+80,y+30)
Mouse.moveTo(x+100,y+0)
Mouse.moveTo(x+100,y+50)
Mouse.moveTo(x+80,y+100)
Mouse.mouseUp()

time.sleep(0.5)
# Letter Р
x = 170
y = 150
Mouse.mouseDown(x+0,y+0)
Mouse.moveTo(x+0,y+100)
Mouse.moveTo(x+0,y+0)
Mouse.moveTo(x+25,y+25)
Mouse.moveTo(x+0,y+50)
Mouse.mouseUp()

time.sleep(0.5)
# Letter О
x = 220
y = 150
Mouse.mouseDown(x+25,y+0)
Mouse.moveTo(x+50,y+50)
Mouse.moveTo(x+25,y+100)
Mouse.moveTo(x+0,y+50)
Mouse.moveTo(x+25,y+0)
Mouse.mouseUp()

time.sleep(0.5)
# Letter Б
x = 290
y = 150
Mouse.mouseDown(x+0,y+0)
Mouse.moveTo(x+50,y+0)
Mouse.moveTo(x+0,y+0)
Mouse.moveTo(x+0,y+100)
Mouse.moveTo(x+0,y+50)
Mouse.moveTo(x+25,y+50)
Mouse.moveTo(x+50,y+75)
Mouse.moveTo(x+25,y+100)
Mouse.moveTo(x+0,y+100)
Mouse.mouseUp()

time.sleep(0.5)
# Letter О
x = 360
y = 150
Mouse.mouseDown(x+25,y+0)
Mouse.moveTo(x+50,y+50)
Mouse.moveTo(x+25,y+100)
Mouse.moveTo(x+0,y+50)
Mouse.moveTo(x+25,y+0)
Mouse.mouseUp()

time.sleep(0.5)
# Letter Т
x = 430
y = 150
Mouse.mouseDown(x+0,y+0)
Mouse.moveTo(x+100,y+0)
Mouse.moveTo(x+50,y+0)
Mouse.moveTo(x+50,y+100)
Mouse.mouseUp()

time.sleep(0.5)
# Letter :)
x = 630
y = 150
Mouse.mouseDown(x+0,y+0)
Mouse.moveTo(x+0,y+75)
Mouse.mouseUp()

Mouse.mouseDown(x+75,y+0)
Mouse.moveTo(x+75,y+75)
Mouse.mouseUp()

Mouse.mouseDown(x-30,y+90)
Mouse.moveTo(x+40,y+130)
Mouse.moveTo(x+105,y+90)
Mouse.mouseUp()