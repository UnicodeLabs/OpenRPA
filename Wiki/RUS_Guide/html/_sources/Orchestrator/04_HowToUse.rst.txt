#################################
4. Как использовать?
#################################

******************************
Как запустить?
******************************

Хотите выполнить запуск Оркестратора?

**Для этого достаточно (выбрать одно):**

- запустить демо-стэнд: запустить .cmd файл, расположенный в папке pyOpenRPA по адресу: Orchestrator\start.cmd (для Windows) и start.sh (для Linux). Далее перейти в браузер по адресу: http://localhost:1024
- в свой .py скрипт добавить следующий код (см. ниже)

.. code-block:: python

    if __name__ == "__main__": # New init way - allow run as module -m PyOpenRPA.Orchestrator
        from pyOpenRPA import Orchestrator  # Import orchestrator main
        gSettings = SettingsTemplate.Create(inModeStr="BASIC") # Create GSettings with basic configuration - no more config is available from the box - you can create own
        # Call the orchestrator main def
        Orchestrator.Orchestrator(inGSettings=gSettings)


******************************
Конфигурационный файл config.py
******************************

Также вы можете выполнить более тонкую настройку параметров Оркестратора. Ниже пример такой настройки:

.. include:: ../../../Orchestrator/config.py
   :literal:


******************************
Быстрая навигация
******************************

- `Сообщество pyOpenRPA (telegram) <https://t.me/pyOpenRPA>`_
- `Сообщество pyOpenRPA (tenchat) <https://tenchat.ru/iMaslov?utm_source=19f2a84f-3268-437f-950c-d987ae42af24>`_
- `Сообщество pyOpenRPA (вконтакте) <https://vk.com/pyopenrpa>`_
- `Презентация pyOpenRPA <https://pyopenrpa.ru/Index/pyOpenRPA_product_service.pdf>`_
- `Портал pyOpenRPA <https://pyopenrpa.ru>`_
- `Репозиторий pyOpenRPA <https://gitlab.com/UnicodeLabs/OpenRPA>`_