.. _module.robot.keyboard:

####################################
4. Функции Keyboard
####################################

************************
Общее
************************

Клавиатура - это главный текстовый инструмент, который обладает 100% точностью передачи данных. С его помощью можно отправлять сообщения, ожидать нажатия и выполнять различные комбинации клавиш. На этой странице представлена вся необходимая информация по управлению клавиатурой со стороны программного робота RPA.

В отличие от многих RPA платформ, pyOpenRPA обладает функциями, которые не зависят от текущей раскладки клавиатуры. За счет этого надежность и стабильность программного робота существенно возрастает.

ВНИМАНИЕ! ПРИ ВЫЗОВЕ ФУНКЦИЙ ОБРАЩАЙТЕ ВНИМАНИЕ НА РЕГИСТР.

************************
Доп. настройки в LINUX
************************
Используется компонент setxkbmap: apt-get install x11-xkb-utils (компонент для взаимодействия с клавиатурой, https://command-not-found.com/setxkbmap)

Особенности ОС Linux позволяют выполнять ввод разноязычного текста только через переключение раскладок клавиатуры в режиме реального времени
По умолчанию установлены следующие раскладки для Русского и английского языков:
Keyboard.KEY_RUS_LAYOUT = "ru" # NEED FOR LINUX (FOR LAYOUT SWITCH)
Keyboard.KEY_ENG_LAYOUT = "us" # NEED FOR LINUX (FOR LAYOUT SWITCH)

Проверить, что данные раскладки работают корректно,  можно с помощью следующей команды в терминале:
setxkbmap -layout us,ru -option grp:alt_shift_toggle
После ввода попробуйте ввести английские символы, после чего переключиться на другой язык с помощью комбинации клавиш Alt+Shift

Если у вас используются другие layout, то вы можете указать их в переменных Keyboard.KEY_RUS_LAYOUT и Keyboard.KEY_ENG_LAYOUT для русского и английского языков соответственно.

**************************************************
Примеры использования
**************************************************

.. code-block:: python

	# Keyboard: Взаимодействие с клавиатурой
	from pyOpenRPA.Robot import Keyboard
	Keyboard.HotkeyCombination(Keyboard.KEY_HOT_WIN_LEFT,Keyboard.KEY_ENG_R)
	Keyboard.Write("cmd")
	Keyboard.Send(Keyboard.KEY_HOT_ENTER, inWaitAfterSecFloat=0.6)
	Keyboard.Write("echo %time%")
	Keyboard.Send(Keyboard.KEY_HOT_ENTER)
	Keyboard.HotkeyCombination(Keyboard.KEY_HOT_CTRL_LEFT, Keyboard.KEY_ENG_A, inWaitAfterSecFloat=0.6)
	Keyboard.HotkeyCombination(Keyboard.KEY_HOT_CTRL_LEFT, Keyboard.KEY_ENG_C, inWaitAfterSecFloat=0.6)
	
Коды клавиш см. ниже

**************************************************
Описание функций
**************************************************

Описание каждой функции начинается с обозначения L+,W+, что означает, что функция поддерживается в ОС Linux (L) и Windows (W)

.. automodule:: pyOpenRPA.Robot.Keyboard
    :members:
    :autosummary:


**************************************************
Коды клавиш
**************************************************
Ниже представлены коды горячих клавиш, а также символов русской и английской раскладки.

.. code-block:: python

	# ШЕСТНАДЦАТИРИЧНЫЙ СКАН-КОД В РУССКОЙ РАСКЛАДКЕ (НЕЗАВИСИМО ОТ ВЫБРАННОГО ЯЗЫКА НА КЛАВИАТУРЕ) 
	# ОТОБРАЖЕНИЕ СКАН КОДОВ НА КЛАВИАТУРЕ https://snipp.ru/handbk/scan-codes

	KEY_RUS_Ф = 0x1E #A
	KEY_RUS_И = 0x30 #B
	KEY_RUS_С = 0x2E #C
	KEY_RUS_В = 0x20 #D
	KEY_RUS_У = 0x12 #E
	KEY_RUS_А = 0x21 #F
	KEY_RUS_П = 0x22 #G
	KEY_RUS_Р = 0x23 #H
	KEY_RUS_Ш = 0x17 #I
	KEY_RUS_О = 0x24 #J
	KEY_RUS_Л = 0x25 #K
	KEY_RUS_Д = 0x26 #L
	KEY_RUS_Ь = 0x32 #M
	KEY_RUS_Т = 0x31 #N
	KEY_RUS_Щ = 0x18 #O
	KEY_RUS_З = 0x19 #P
	KEY_RUS_Й = 0x10 #Q
	KEY_RUS_К = 0x13 #R
	KEY_RUS_Ы = 0x1F #S
	KEY_RUS_Е = 0x14 #T
	KEY_RUS_Г = 0x16 #U
	KEY_RUS_М = 0x2F #V
	KEY_RUS_Ц = 0x11 #W
	KEY_RUS_Ч = 0x2D #X
	KEY_RUS_Н = 0x15 #Y
	KEY_RUS_Я = 0x2C #Z
	KEY_RUS_Ё = 0x29 #~
	KEY_RUS_Ж = 0x27 #:
	KEY_RUS_Б = 0x33 #<
	KEY_RUS_Ю = 0x34 #>
	KEY_RUS_Х = 0x1A #[
	KEY_RUS_Ъ = 0x1B #]
	KEY_RUS_Э = 0x28 #'

	KEY_ENG_A = 0x1E #A
	KEY_ENG_B = 0x30 #B
	KEY_ENG_C = 0x2E #C
	KEY_ENG_D = 0x20 #D
	KEY_ENG_E = 0x12 #E
	KEY_ENG_F = 0x21 #F
	KEY_ENG_G = 0x22 #G
	KEY_ENG_H = 0x23 #H
	KEY_ENG_I = 0x17 #I
	KEY_ENG_J = 0x24 #J
	KEY_ENG_K = 0x25 #K
	KEY_ENG_L = 0x26 #L
	KEY_ENG_M = 0x32 #M
	KEY_ENG_N = 0x31 #N
	KEY_ENG_O = 0x18 #O
	KEY_ENG_P = 0x19 #P
	KEY_ENG_Q = 0x10 #Q
	KEY_ENG_R = 0x13 #R
	KEY_ENG_S = 0x1F #S
	KEY_ENG_T = 0x14 #T
	KEY_ENG_U = 0x16 #U
	KEY_ENG_V = 0x2F #V
	KEY_ENG_W = 0x11 #W
	KEY_ENG_X = 0x2D #X
	KEY_ENG_Y = 0x15 #Y
	KEY_ENG_Z = 0x2C #Z


	KEY_HOT_NUMPAD_0 = 0x52
	KEY_HOT_NUMPAD_1 = 0x4F
	KEY_HOT_NUMPAD_2 = 0x50
	KEY_HOT_NUMPAD_3 = 0x51
	KEY_HOT_NUMPAD_4 = 0x4B
	KEY_HOT_NUMPAD_5 = 0x4C
	KEY_HOT_NUMPAD_6 = 0x4D
	KEY_HOT_NUMPAD_7 = 0x47
	KEY_HOT_NUMPAD_8 = 0x48
	KEY_HOT_NUMPAD_9 = 0x49
	KEY_HOT_NUMPAD_ASTERISK = 0x37 #*
	KEY_HOT_NUMPAD_PLUS = 0x4E
	KEY_HOT_NUMPAD_MINUS = 0x4A
	KEY_HOT_NUMPAD_DELETE = 0x53
	KEY_HOT_NUMPAD_SOLIDUS = 0x35 #/
	KEY_HOT_NUMPAD_ENTER = 0x11c

	KEY_HOT_F1 = 0x3B
	KEY_HOT_F2 = 0x3C
	KEY_HOT_F3 = 0x3D
	KEY_HOT_F4 = 0x3E
	KEY_HOT_F5 = 0x3F
	KEY_HOT_F6 = 0x40
	KEY_HOT_F7 = 0x41
	KEY_HOT_F8 = 0x42
	KEY_HOT_F9 = 0x43
	KEY_HOT_F10 = 0x44
	KEY_HOT_F11 = 0x57
	KEY_HOT_F12 = 0x58
	KEY_HOT_F13 = 0x7C
	KEY_HOT_F14 = 0x7D
	KEY_HOT_F15 = 0x7E
	KEY_HOT_F16 = 0x7F
	KEY_HOT_F17 = 0x80
	KEY_HOT_F18 = 0x81
	KEY_HOT_F19 = 0x82
	KEY_HOT_F20 = 0x83
	KEY_HOT_F21 = 0x84
	KEY_HOT_F22 = 0x85
	KEY_HOT_F23 = 0x86
	KEY_HOT_F24 = 0x87

	KEY_HOT_TILDE = 0x29 #~
	KEY_HOT_COLON = 0x27 #:
	KEY_HOT_PLUS = 0x0D #+
	KEY_HOT_MINUS = 0x0C #-
	KEY_HOT_LESS_THAN = 0x33 #< ,
	KEY_HOT_GREATER_THAN = 0x34 #> .
	KEY_HOT_SOLIDUS = 0x35 #/ ?
	KEY_HOT_SQUARE_BRACKET_LEFT = 0x1A #[
	KEY_HOT_SQUARE_BRACKET_RIGHT = 0x1B #]
	KEY_HOT_APOSTROPHE = 0x28 #' "
	KEY_HOT_VERTICAL_LINE = 0x2B #| \

	KEY_HOT_ESC = 0x1
	KEY_HOT_BACKSPACE = 0x0E
	KEY_HOT_TAB = 0x0F
	KEY_HOT_ENTER = 0x1C
	KEY_HOT_CONTEXT_MENU = 0x15D
	KEY_HOT_SHIFT_LEFT = 0x2A
	KEY_HOT_SHIFT_RIGHT = 0x36
	KEY_HOT_CTRL_LEFT = 0x1D
	KEY_HOT_CTRL_RIGHT = 0x11D
	KEY_HOT_ALT_LEFT = 0x38
	KEY_HOT_ALT_RIGHT = 0x138
	KEY_HOT_WIN_LEFT = 0x5B
	KEY_HOT_WIN_RIGHT = 0x5C
	KEY_HOT_CAPS_LOCK = 0x3A
	KEY_HOT_NUM_LOCK = 0x45
	KEY_HOT_SCROLL_LOCK = 0x46
	KEY_HOT_END = 0x4F
	KEY_HOT_HOME = 0x47
	KEY_HOT_SPACE = 0x39
	KEY_HOT_PAGE_UP = 0x49
	KEY_HOT_PAGE_DOWN = 0x51
	KEY_HOT_CLEAR = 0x4C
	KEY_HOT_LEFT = 0x4B
	KEY_HOT_UP = 0x48
	KEY_HOT_RIGHT = 0x4D
	KEY_HOT_DOWN = 0x50
	KEY_HOT_PRINT_SCREEN = 0x137
	KEY_HOT_INSERT = 0x52
	KEY_HOT_DELETE = 0x53

	KEY_HOT_0 = 0xB
	KEY_HOT_1 = 0x2
	KEY_HOT_2 = 0x3
	KEY_HOT_3 = 0x4
	KEY_HOT_4 = 0x5
	KEY_HOT_5 = 0x6
	KEY_HOT_6 = 0x7
	KEY_HOT_7 = 0x8
	KEY_HOT_8 = 0x9
	KEY_HOT_9 = 0xA
	
**************************************************
Дополнительная функциональность
**************************************************
Дополнительно модуль содержит функции вспомогательной библиотеки. Ознакомиться с описанием можно `Здесь <https://github.com/boppreh/keyboard#api>`_

.. code-block:: python

	# Пример использования функции send
	from pyOpenRPA.Robot import Keyboard
	Keyboard.send(57)

******************************
Быстрая навигация
******************************

- `Сообщество pyOpenRPA (telegram) <https://t.me/pyOpenRPA>`_
- `Сообщество pyOpenRPA (tenchat) <https://tenchat.ru/iMaslov?utm_source=19f2a84f-3268-437f-950c-d987ae42af24>`_
- `Сообщество pyOpenRPA (вконтакте) <https://vk.com/pyopenrpa>`_
- `Презентация pyOpenRPA <https://pyopenrpa.ru/Index/pyOpenRPA_product_service.pdf>`_
- `Портал pyOpenRPA <https://pyopenrpa.ru>`_
- `Репозиторий pyOpenRPA <https://gitlab.com/UnicodeLabs/OpenRPA>`_

