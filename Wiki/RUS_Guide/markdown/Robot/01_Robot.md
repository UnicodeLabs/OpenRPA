# 1. Описание

## Общее

Модуль обеспечивает всю необходимую функциональность для создания любого программного робота RPA. Модуль робота поставляется в качестве библиотеки Python, что позволяет с легкостью интегрировать его в другие проекты перспективных технологий.

**Содержит**


* **Уровень доступа к элементам локального приложения (win32, UI automation), и веб приложения**


    * UIDesktop: инструменты взаимодействия с элементами локального приложения (взаимодействие с ОС через протоколы win32, UI automation). Перейти к описанию функций: 2. Функции UIDesktop


    * UIWeb: инструменты взаимодействия с элементами веб приложения. Перейти к описанию функций: 3. Функции UIWeb


* **Уровень доступа к текстовым каналам передачи данных (клавиатура, буфер обмена)**


    * Keyboard: инструменты взаимодействия с клавиатурой. Перейти к описанию функций: 4. Функции Keyboard


    * Clipboard: инструменты взаимодействия с буфером обмена. Перейти к описанию функций: 5. Функции Clipboard


* **Уровень доступа к графическим каналам передачи данных (мышь, экран)**


    * Mouse: инструменты взаимодействия с мышью. Перейти к описанию функций: 6. Функции Mouse


    * Screen: инструменты взаимодействия с эраном рабочего стола. Перейти к описанию функций: 7. Функции Screen


* **Уровень доступа к звуковым каналам передачи данных (микрофон, динамик)**


    * Audio: инструменты взаимодействия с аудио. Перейти к описанию функций: 8. Функции Audio

Дорогие коллеги!

Мы знаем, что с pyOpenRPA вы сможете существенно улучшить качество вашего бизнеса. Платформа роботизации pyOpenRPA - это разработка, которая дает возможность делать виртуальных сотрудников (программных роботов RPA) выгодными, начиная от эффекта всего в **10 тыс. руб.** И управлять ими будете только Вы!

Если у вас останутся вопросы, то вы всегда можете обратиться в центр поддержки клиентов pyOpenRPA. Контакты: 2. Лицензия & Контакты

pyOpenRPA - роботы помогут!

## Примеры

**Ниже преставлен пример использования инструментов робота.**

```
import time
from pyOpenRPA.Robot import UIDesktop

# UIDesktop: Работа с 1С
lDemoBaseSelector = [{"title":"Запуск 1С:Предприятия","class_name":"V8TopLevelFrameTaxiStarter","backend":"uia"},{"title":"DEMO", "depth_start": 5, "depth_end": 5}]
lDemoBase = UIDesktop.UIOSelector_Get_UIO(lDemoBaseSelector)
lDemoBase.draw_outline()
time.sleep(2.0)
lRunBaseSelector = [{"title":"Запуск 1С:Предприятия","class_name":"V8TopLevelFrameTaxiStarter","backend":"uia"},{"title":"1С:Предприятие", "depth_start": 4, "depth_end": 4}]
lRunBase = UIDesktop.UIOSelector_Get_UIO(lRunBaseSelector)
lRunBase.draw_outline()
time.sleep(2.0)
lRunBase.click_input()

# ОТКРЫТЬ ЗАКАЗЫ ПОКУПАТЕЛЕЙ
lOrderNumberSelector = [{"title":"Управление нашей фирмой, редакция 1.6","class_name":"V8TopLevelFrameSDI","backend":"uia"},{"title":"АСФР-000036 Номер", "depth_start": 13, "depth_end": 13}]
UIDesktop.UIOSelector_Get_UIO(lOrderNumberSelector).draw_outline()
UIDesktop.UIOSelector_Get_UIO(lOrderNumberSelector).double_click_input()

time.sleep(1.0)
lCommentSelector = [{"title":"Управление нашей фирмой, редакция 1.6","class_name":"V8TopLevelFrameSDI","backend":"uia"},{"control_type":"Pane"},{"control_type":"Pane"},{"control_type":"Pane"},{"control_type":"Pane"},{"control_type":"Pane"},{"control_type":"Pane"},{"control_type":"Pane"},{"title":"","control_type":"Edit", "depth_start": 1, "depth_end": 10}]
UIDesktop.UIOSelector_Get_UIO(lCommentSelector).draw_outline()
UIDesktop.UIOSelector_Get_UIO(lCommentSelector).set_edit_text("Заказ исполнен роботом")

# UIWeb: Работа с браузером
# WIKI TO DO

# Keyboard: Взаимодействие с клавиатурой
import ctypes  # An included library with Python install.
from pyOpenRPA.Robot import Keyboard
from pyOpenRPA.Robot import Clipboard
Keyboard.send("win+r")
time.sleep(0.3)
Keyboard.write("cmd")
time.sleep(0.3)
Keyboard.send("enter")
time.sleep(0.6)
Keyboard.write("echo %time%")
time.sleep(0.3)
Keyboard.send("enter")
time.sleep(0.3)
Keyboard.send("ctrl+a")
time.sleep(0.6)
Clipboard.ClipboardSet("")
Keyboard.send("ctrl+c")
time.sleep(0.6)
lTextRaw = Clipboard.ClipboardGet()
lTimeStr = lTextRaw.split("\n")[-3]

def msg_box(title, text, style):
        return ctypes.windll.user32.MessageBoxW(0, text, title, style)
msg_box('Робот на клавиатуре', f'Робот извлек время из консоли: {lTimeStr}', 0)

# Mouse: Взаимодействие с мышью
from pyOpenRPA.Robot import Mouse
# Нарисовать букву Я
x = -50
y = 150
Mouse.mouseDown(x+100,y+0)
Mouse.moveTo(x+100,y+100)
Mouse.moveTo(x+100,y+50)
Mouse.moveTo(x+80,y+30)
Mouse.moveTo(x+100,y+0)
Mouse.moveTo(x+100,y+50)
Mouse.moveTo(x+80,y+100)
Mouse.mouseUp()

time.sleep(0.5)
# Нарисовать :)
x = 230
y = 150
Mouse.mouseDown(x+0,y+0)
Mouse.moveTo(x+0,y+75)
Mouse.mouseUp()

Mouse.mouseDown(x+75,y+0)
Mouse.moveTo(x+75,y+75)
Mouse.mouseUp()

Mouse.mouseDown(x-30,y+90)
Mouse.moveTo(x+40,y+130)
Mouse.moveTo(x+105,y+90)
Mouse.mouseUp()
```

## Быстрая навигация


* [Сообщество pyOpenRPA (telegram)](https://t.me/pyOpenRPA)


* [Сообщество pyOpenRPA (tenchat)](https://tenchat.ru/iMaslov?utm_source=19f2a84f-3268-437f-950c-d987ae42af24)


* [Сообщество pyOpenRPA (вконтакте)](https://vk.com/pyopenrpa)


* [Презентация pyOpenRPA](https://pyopenrpa.ru/Index/pyOpenRPA_product_service.pdf)


* [Портал pyOpenRPA](https://pyopenrpa.ru)


* [Репозиторий pyOpenRPA](https://gitlab.com/UnicodeLabs/OpenRPA)

		.. v1.3.1 replace:: v1.3.1
