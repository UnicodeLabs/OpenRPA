# 8. Функции Audio

!ВНИМАНИЕ! ДЛЯ КОРРЕКТНОЙ РАБОТЫ МОДУЛЯ ТРЕБУЕТСЯ КОМПОНЕНТ ffmpeg.exe (Для Windows x64 можно найти в pyOpenRPAResourcesWAudio). На него должен указывать один из путей в переменной окружения PATH. Или ffmpeg.exe должен быть расположен в рабочей директории (working directory)

!ВНИМАНИЕ! ДЛЯ ВИРТУАЛЬНОЙ МАШИНЫ МОЖЕТ ПОТРЕБОВАТЬСЯ КОМПОНЕНТ ВИРТУАЛЬНОГО АУДИОДРАЙВЕРА (Для Windows x64 можно найти в pyOpenRPAResourcesWAudioVBCABLE_Driver_Pack43.zip)

## Общее

Дорогие коллеги!

Мы знаем, что с pyOpenRPA вы сможете существенно улучшить качество вашего бизнеса. Платформа роботизации pyOpenRPA - это разработка, которая дает возможность делать виртуальных сотрудников (программных роботов RPA) выгодными, начиная от эффекта всего в **10 тыс. руб.** И управлять ими будете только Вы!

Если у вас останутся вопросы, то вы всегда можете обратиться в центр поддержки клиентов pyOpenRPA. Контакты: 2. Лицензия & Контакты

pyOpenRPA - роботы помогут!

## Класс Recorder

Экземпляр класса pyOpenRPA.Robot.Audio.Recorder, который обеспечивает захват звука (с микрофона или из приложений) и сохраняет в виде аудиофайла (множества аудиофайлов)

## Описание функций

Описание каждой функции начинается с обозначения L-,W+, что означает, что функция не поддерживается в ОС Linux (L) и поддерживается в Windows (W)

**Functions:**

| `DeviceListGet`()

 | L-,W+: Вернуть список аудио устройст (входящих и исходящих, микрофонов и динамиков).

 |
| `DeviceMicrophoneIndex`()

                           | L-,W+: Выполнить поиск устройства, с помощью которого можно будет выполнить захват c микрофона.

                                                                                                                |
| `DeviceSystemSoundIndex`()

                          | L-,W+: Выполнить поиск устройства, с помощью которого можно будет выполнить захват аудио, которое поступает из приложений.

                                                                                     |

### pyOpenRPA.Robot.Audio.DeviceListGet()
L-,W+: Вернуть список аудио устройст (входящих и исходящих, микрофонов и динамиков).

from pyOpenRPA.Robot import Audio
Audio.DeviceListGet()


* **Результат**

    [{«IndexInt»:1, «NameStr»: «», 

        «HostApiInt»: 0, «HostApiStr»: «MME»|»Windows WASAPI»|»Windows WDM-KS»,
        «MaxInputChannelsInt»: 0, «MaxOutputChannelsInt»: 0,
        «DefaultSampleRateFloat»: 44100.0

    },…]




* **Тип результата**

    list



### pyOpenRPA.Robot.Audio.DeviceMicrophoneIndex()
L-,W+: Выполнить поиск устройства, с помощью которого можно будет выполнить захват c микрофона.


### pyOpenRPA.Robot.Audio.DeviceSystemSoundIndex()
L-,W+: Выполнить поиск устройства, с помощью которого можно будет выполнить захват аудио, которое поступает из приложений. Например: аудиоконференции Zoom, whatsapp, telegram и т.д.


### class pyOpenRPA.Robot.Audio.Recorder(inDeviceInt=None)
**Methods:**

| `CaptureChunk`([inExtra, inForceChunkBool, …])

      | L-,W+: Зафиксировать захват аудио в виде промежуточного файла вида: <имя файла>_00000.mp3

                                                                                                                      |
| `CaptureStart`([inFolderPathStr, …])

                | L-,W+: Начать запись звука

                                                                                                                                                                                     |
| `CaptureStop`([inWaitStream, inExtra])

              | L-,W+: Остановить захват аудио

                                                                                                                                                                                 |
| `CaptureWait`([inWaitCallbackChunkBool, …])

         | L-,W+: Ожидать окончания захвата аудио.

                                                                                                                                                                        |
| `FileInfoGet`([inFileNameStr])

                      | L-,W+: Вернуть информацию по аудиофайлу inFileNameStr.

                                                                                                                                                         |
| `FileLastGet`()

                                     | L-,W+: Вернуть наименование последнего сохраненного аудиофайла

                                                                                                                                                 |
| `FileListGet`()

                                     | L-,W+: Вернуть список сохраненных аудиофайлов (наименования)

                                                                                                                                                   |
| `StatusGet`()

                                       | L-,W+: Вернуть статус записи звука

                                                                                                                                                                             |

#### CaptureChunk(inExtra=None, inForceChunkBool=True, inShiftSecFloat=0.0)
L-,W+: Зафиксировать захват аудио в виде промежуточного файла вида: <имя файла>_00000.mp3


* **Параметры**

    
    * **inExtra** (*any**, **опционально*) – Дополнительный контент, необходимый для идентификации файла. В дальнейшем получить структуру можно с помощью функции FileInfoGet()[„Extra“], по умолчанию None


    * **inForceChunkBool** (*bool**, **опционально*) – True - вне зависимости от текущего режима перейти на режим сохранения по частям, по умолчанию True


    * **inShiftSecFloat** (*float*) – Последние секунды, которые не записывать в промежуточный аудиофайл. Они будут началом следующего аудио отрывка, по умолчанию 0.0



* **Результат**

    Наименование сохраненного аудиофайла



* **Тип результата**

    str



#### CaptureStart(inFolderPathStr='', inFileNameStr='out', inFileFormatStr='mp3', inDurationSecFloat=None, inChunkSecFloat=300.0, inCallbackChunkDef=None, inCallbackStopDef=None)
L-,W+: Начать запись звука

```
def CallbackChunk(lRec, lFilenameStr):
    pass # КОД ОБРАБОТКИ ПОСЛЕ СОХРАНЕНИЯ ЧАСТИ

from pyOpenRPA.Robot import Audio
lRec = Audio.Recorder()
lRec.CaptureStart(inFileNameStr = "out", inFileFormatStr = "mp3", inDurationSecFloat = None, inChunkSecFloat = 5.0, inCallbackChunkDef=CallbackChunk)
lRec.CaptureStop()
```


* **Параметры**

    
    * **inFolderPathStr** (*str**, **опционально*) – Путь к папке, в которую сохранять аудиофайлы захвата , по умолчанию «»


    * **inFileNameStr** (*str**, **опционально*) – Наименование файла без расширения, по умолчанию «out»


    * **inFileFormatStr** (*str**, **опционально*) – Наименование формата, в который будет происходить сохранение («mp3» или «wav» или «raw» или «aif»), по умолчанию «mp3»


    * **inDurationSecFloat** (*float**, **опционально*) – Длительность захвата аудио, по умолчанию None (пока не поступит команда CaptureStop() )


    * **inChunkSecFloat** (*float**, **опционально*) – Максимальная длина части аудиофайла, по умолчанию 300.0


    * **inCallbackChunkDef** (*def**, **опционально*) – Функция, которая будет инициирована в случае выполнения Chunk сохранения (сохранение части). Callback функция должна принимать 2 аргумента: экземпляр класса Recorder и наименование сохраненного файла. Внимание! Функция запускается асинхронно!


    * **inCallbackStopDef** (*def**, **опционально*) – Функция, которая будет инициирована в случае окончания записи. Callback функция должна принимать 2 аргумента: экземпляр класса Recorder и наименование сохраненного файла. Внимание! Функция запускается асинхронно!



#### CaptureStop(inWaitStream=True, inExtra=None)
L-,W+: Остановить захват аудио


* **Параметры**

    
    * **inWaitStream** (*bool**, **опционально*) – True - выполнить ожидание окончания потока захвата перед окончанием, по умолчанию True


    * **inExtra** (*any**, **опционально*) – Дополнительный контент, необходимый для идентификации файла. В дальнейшем получить структуру можно с помощью функции FileInfoGet()[„Extra“], по умолчанию None



#### CaptureWait(inWaitCallbackChunkBool=True, inWaitCallbackStopBool=True)
L-,W+: Ожидать окончания захвата аудио. Дополнительно настраивается ожидание окончания всех callback функций.


* **Параметры**

    
    * **inWaitCallbackChunkBool** (*bool**, **опционально*) – True - ожидать выполнение всех асинхронных callback по сохранению части аудиофайла, по умолчанию True


    * **inWaitCallbackStopBool** (*bool**, **опционально*) – True - ожидать выполнение всех асинхронных callback по завершению записи, по умолчанию True



#### FileInfoGet(inFileNameStr=None)
L-,W+: Вернуть информацию по аудиофайлу inFileNameStr. Если inFileNameStr == None -> Функция вернет информацию по последнему записанному файлу


* **Параметры**

    **inFileNameStr** (*str**, **опционально*) – Наименование аудиофайла с указанием расширения, по умолчанию None (взять последний записанный файл)



* **Результат**

    {StartSecFloat:, EndSecFloat:, Extra:, PathStr:, } или None



* **Тип результата**

    dict



#### FileLastGet()
L-,W+: Вернуть наименование последнего сохраненного аудиофайла


* **Результат**

    [«out_00000.mp3», «out_00001.mp3», …]



* **Тип результата**

    list



#### FileListGet()
L-,W+: Вернуть список сохраненных аудиофайлов (наименования)


* **Результат**

    [«out_00000.mp3», «out_00001.mp3», …]



* **Тип результата**

    list



#### StatusGet()
L-,W+: Вернуть статус записи звука


* **Результат**

    «0_READY» или «1_RECORDING»



* **Тип результата**

    str


## Быстрая навигация


* [Сообщество pyOpenRPA (telegram)](https://t.me/pyOpenRPA)


* [Сообщество pyOpenRPA (tenchat)](https://tenchat.ru/iMaslov?utm_source=19f2a84f-3268-437f-950c-d987ae42af24)


* [Сообщество pyOpenRPA (вконтакте)](https://vk.com/pyopenrpa)


* [Презентация pyOpenRPA](https://pyopenrpa.ru/Index/pyOpenRPA_product_service.pdf)


* [Портал pyOpenRPA](https://pyopenrpa.ru)


* [Репозиторий pyOpenRPA](https://gitlab.com/UnicodeLabs/OpenRPA)

		.. v1.3.1 replace:: v1.3.1
