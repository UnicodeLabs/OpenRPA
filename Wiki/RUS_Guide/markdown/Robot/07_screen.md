# 7. Функции Screen

## Общее

!ВНИМАНИЕ! ДЛЯ РАБОТЫ В LINUX ТРЕБУЕТСЯ НАЛИЧИЕ КОМПОНЕНТА SCROT (sudo apt-get scrot)

Дорогие коллеги!

Мы знаем, что с pyOpenRPA вы сможете существенно улучшить качество вашего бизнеса. Платформа роботизации pyOpenRPA - это разработка, которая дает возможность делать виртуальных сотрудников (программных роботов RPA) выгодными, начиная от эффекта всего в **10 тыс. руб.** И управлять ими будете только Вы!

Если у вас останутся вопросы, то вы всегда можете обратиться в центр поддержки клиентов pyOpenRPA. Контакты: 2. Лицензия & Контакты

pyOpenRPA - роботы помогут!

## Класс Box

Экземпляр класса pyscreeze.Box, который характеризует прямоугольную область на экране.

top - координата левой верхней точки в пикселях по оси X (горизонталь)
left - координата левой верхней точки в пикселях по оси Y (вертикаль)
height - расстояние вниз от левой верхней точки в пикселях
width - расстояние вправо от левой верхней точки в пикселях

## Класс Point

Экземпляр класса pyscreeze.Point, который характеризует точку на экране.

x - координата точки в пикселях по оси X (горизонталь)
y - координата точки в пикселях по оси Y (вертикаль)

## Символьное указание точки (inPointRuleStr)

### LU|CU|RU

### LC|CC|RC

LD|CD|RD

Символьное указание точки - точка относительно которой выполнить изменение прямоугольной области.

> «CC»,

Формат образования кода: XY, где


* X обозначает положение по горизонтали (допустимые значения: «L», «C», «R»)


* Y обозначает положение по вертикали (допустимые значения: «U», «C», «D»)

Допустимые значения:


* «CC» - центр по горизонтали, центр по вертикали


* «LU» - левый край по горизонтали, верхний край по вертикали


* «LD» - левый край по горизонтали, нижний край по вертикали


* «RD» - правый край по горизонтали, нижний край по вертикали


* «RU» - правый край по горизонтали, верхний край по вертикали

X-10 - корректировка координаты по оси X на 10 пикселей влево
Y+20 - корректировка координаты по оси Y на 20 пикселей вниз

## Символьное указание области (inAnchorRuleStr)

### LU|CU|RU

### LC|CC|RC

LD|CD|RD

Символьное указание области поиска - область относительно которой выполнить поиск другой прямоугольной области.

> «CC»,

Формат образования кода: XY, где


* X обозначает область по горизонтали (допустимые значения: «L», «C», «R»)


* Y обозначает область по вертикали (допустимые значения: «U», «C», «D»)

Допустимые значения:


* «CC» - выбранная прямоугольная область


* «LU» - слева и сверху от выбранной прямоугольной области


* «LD» - слева и снизу от выбранной прямоугольной области


* «LС» - слева от выбранной прямоугольной области


* «RC» - справа от выбранной прямоугольной области


* «СU» - сверху от выбранной прямоугольной области


* «CD» - сверху от выбранной прямоугольной области


* «RD» - справа и снизу от выбранной прямоугольной области


* «RU» - справа и сверху от выбранной прямоугольной области

Опция «S» (strict) - искомый объект должен всеми своими координатами находиться в обозначенной прямоугольной области

Формат допускает комбинации нескольких областей в одной строке.
Пример:
«CC,LU,LD,S»
«CC|LU|LD|S»
«CCLULDS»

Графическая интерпретация:
+|-

```
|-
-----
-|+|
```

-
—–
+|-

```
|
```

-

## Описание функций

Описание каждой функции начинается с обозначения L+,W+, что означает, что функция поддерживается в ОС Linux (L) и Windows (W)

**Functions:**

| `BoxAnchorRuleCheck`(inBox[, inAnchorBox, …])

 | L+,W+: Выполнить проверку соответствия всем условиям вхождения inBox в inAnchorBox с учетом правил inAnchorRule

 |
| `BoxCreate`(inTopInt, inLeftInt, inHeightInt, …)

    | L+,W+: Создать экземпляр прямоугольной области.

                                                                                                                                                                |
| `BoxDraw`(inBox[, inColorStr, inThicknessInt])

      | L+,W+: Выполнить подсветку прямоугольной области inBox на экране

                                                                                                                                               |
| `BoxGetPoint`(inBox[, inPointRuleStr])

              | L+,W+:Получить точку pyscreeze.Point по заданной прямоугольной области pyscreeze.Box и строковому параметру расположения inPointRuleStr.

                                                                       |
| `BoxModify`(inBox[, inDWidthInt, …])

                | L+,W+: Изменить ширину / высоту прямоугольной области.

                                                                                                                                                         |
| `BoxMoveTo`(inBox[, inDXInt, inDYInt])

              | L+,W+: Переместить прямоугольную область (сохранить длину/ширину).

                                                                                                                                             |
| `BoxOverlay`(inBox1, inBox2)

                        | L+,W+:Проверить наложение 2-х прямоугольных областей друг на друга.

                                                                                                                                            |
| `ImageClick`(inImgPathStr[, inBoxIndexInt, …])

      | L+,W+:Выполнить поиск прямоугольной области по изображению.

                                                                                                                                                    |
| `ImageExists`(inImgPathStr[, …])

                    | L+,W+:Проверить, имеется ли на экране хотя бы один подходящий объект.

                                                                                                                                          |
| `ImageLocateAll`(inImgPathStr[, …])

                 | L+W+: Искать на экране графические объекты, которые похожи на inImgPathStr.

                                                                                                                                    |
| `ImageWaitAppear`(inImgPathStr[, …])

                | L+,W+:Ожидать появление изображения на протяжении inWaitSecFloat секунд.

                                                                                                                                       |
| `ImageWaitDisappear`(inImgPathStr[, …])

             | L+,W+:Ожидать исчезновение изображения на протяжении inWaitSecFloat секунд.

                                                                                                                                    |
| `PointClick`(inPoint[, inClickCountInt, …])

         | L+,W+:Нажатие (вниз) кнопки мыши и затем немедленно выпуск (вверх) её.

                                                                                                                                         |
| `PointClickDouble`(inPoint[, inWaitAfterSecFloat])

  | L+,W+:Двойное нажатие левой клавиши мыши.

                                                                                                                                                                      |
| `PointCreate`(inXInt, inYInt)

                       | L+,W+:Создать точку pyscreeze.Point.

                                                                                                                                                                           |
| `PointDown`(inPoint[, inButtonStr, …])

              | L+,W+:Переместить указатель по координатам inPoint, после чего нажать (вниз) клавишу мыши и не отпускать до выполнения соответсвующей команды (см.

                                                             |
| `PointModify`(inPoint, inDXInt, inDYInt)

            | L+,W+:Скорректировать точку pyscreeze.Point.

                                                                                                                                                                   |
| `PointMoveTo`(inPoint[, inWaitAfterSecFloat])

       | L+,W+:Переместить указатель мыши на позицию inXInt, inYInt за время inMoveDurationSecFloat.

                                                                                                                    |
| `PointUp`(inPoint[, inButtonStr, …])

                | L+,W+:Отпустить (вверх) клавишу мыши.

                                                                                                                                                                          |

### pyOpenRPA.Robot.Screen.BoxAnchorRuleCheck(inBox, inAnchorBox=None, inAnchorRuleStr=None)
L+,W+: Выполнить проверку соответствия всем условиям вхождения inBox в inAnchorBox с учетом правил inAnchorRule

```
# Screen: Взаимодействие с экраном
from pyOpenRPA.Robot import Screen
lBox1 = Screen.BoxCreate(inTopInt=265, inLeftInt=62, inHeightInt=100, inWidthInt=90)
lBox2 = Screen.BoxCreate(inTopInt=160, inLeftInt=160, inHeightInt=100, inWidthInt=100)
lBox3 = Screen.BoxCreate(inTopInt=460, inLeftInt=60, inHeightInt=100, inWidthInt=100)

l = Screen.BoxAnchorRuleCheck(inBox=lBox1, inAnchorBox=[lBox2,lBox3], inAnchorRuleStr=["LD","CUS"])
Screen.BoxDraw([lBox1,lBox2,lBox3])
```


* **Параметры**

    
    * **inBox** (*pyscreeze.Box*) – Экземпляр класса прямоугольной области Box


    * **inAnchorBox** (*pyscreeze.Box или list из pyscreeze.Box*) – Экземпляр класса прямоугольной области Box


    * **inAnchorRuleStr** (*str или list из str*) – Символьное указание области проверки соответствия. Может принимать единственное значение (единый формат для всех inAnchorBox), или список форматов для каждого inAnchorBox (если inAnchorBox является списком Box)



* **Результат**

    True - соответствует всем правилам



* **Тип результата**

    bool



### pyOpenRPA.Robot.Screen.BoxCreate(inTopInt: int, inLeftInt: int, inHeightInt: int, inWidthInt: int)
L+,W+: Создать экземпляр прямоугольной области.

!ВНИМАНИЕ! Координаты inTopInt, inLeftInt определяют верхний левый край прямоугольной области.


* **Параметры**

    
    * **inTopInt** (*int*) – Координата левой верхней точки в пикселях по оси X (горизонталь)


    * **inLeftInt** (*int*) – Координата левой верхней точки в пикселях по оси Y (вертикаль)


    * **inHeightInt** (*int*) – Расстояние вниз от левой верхней точки в пикселях


    * **inWidthInt** (*int*) – Расстояние вправо от левой верхней точки в пикселях



### pyOpenRPA.Robot.Screen.BoxDraw(inBox, inColorStr='green', inThicknessInt=2)
L+,W+: Выполнить подсветку прямоугольной области inBox на экране

!ВНИМАНИЕ! ЦВЕТ inColorStr ПОДДЕРЖИВАЕТСЯ ТОЛЬКО НА ОС WINDOWS

!ВНИМАНИЕ! ПОДДЕРЖИВАЕТ ПАКЕТНУЮ ОБРАТКУ ПРИ ПЕРЕДАЧЕ СПИСКА ЭКЗЕМПЛЯРОВ BOX

```
# Screen: Взаимодействие с экраном
from pyOpenRPA.Robot import Screen
# ВАРИАНТ ОТРИСОВКИ 1ГО ЭЛЕМЕНТА
# Создать пробную прямоугольную область
lBox = Screen.BoxCreate(inTopInt=10, inLeftInt=10, inHeightInt=10, inWidthInt=10)
Screen.BoxDraw(lBox)

# ВАРИАНТ ПАКЕТНОЙ ОТРИСОВКИ
# Создать пробную прямоугольную область
lBox = Screen.BoxCreate(inTopInt=10, inLeftInt=10, inHeightInt=100, inWidthInt=100)
lBox2 = Screen.BoxCreate(inTopInt=60, inLeftInt=60, inHeightInt=100, inWidthInt=100)
Screen.BoxDraw([lBox, lBox2])
```


* **Параметры**

    
    * **inBox** (*pyscreeze.Box*) – Экземпляр класса прямоугольной области Box


    * **inColorStr** (*str**, **необязательный*) – цвет подсветки прямоугольной области. Варианты: „red“, „green“, „blue“. По умолчанию „green“


    * **inThicknessInt** (*int**, **необязательный*) – толщина подсветки прямоугольной области. По умолчанию 2



### pyOpenRPA.Robot.Screen.BoxGetPoint(inBox, inPointRuleStr='CC')
L+,W+:Получить точку pyscreeze.Point по заданной прямоугольной области pyscreeze.Box и строковому параметру расположения inPointRuleStr.

```
# Screen: Взаимодействие с мышью объектами экрана
from pyOpenRPA.Robot import Screen
lBox1 = Screen.BoxCreate(inTopInt=10, inLeftInt=10, inHeightInt=100, inWidthInt=1000)
lPoint = Screen.BoxGetPoint(inBox=lBox1, inPointRuleStr="LC")
```


* **Параметры**

    
    * **inBox** (*pyscreeze.Box**, **обязательный*) – Прямоугольная область на экране


    * **inPointRuleStr** (*str**, **опциональный*) – Правило идентификации точки на прямоугольной области (правила формирования см. выше). Варианты: «LU»,»CU»,»RU»,»LC»,»CC»,»RC»,»LD»,»CD»,»RD».  По умолчанию «CC»



* **Результат**

    Точка на экране



* **Тип результата**

    pyscreeze.Point



### pyOpenRPA.Robot.Screen.BoxModify(inBox, inDWidthInt=None, inDHeightInt=None, inPointRuleStr='CC')
L+,W+: Изменить ширину / высоту прямоугольной области.

!ВНИМАНИЕ! ПОДДЕРЖИВАЕТ ПАКЕТНУЮ ОБРАТКУ ПРИ ПЕРЕДАЧЕ СПИСКА ЭКЗЕМПЛЯРОВ BOX

!ВНИМАНИЕ! ЕСЛИ СМЕЩЕНИЕ ПРИВЕДЕТ К ОБРАЗОВАНИЮ ДРОБНОГО ЧИСЛА, БУДЕТ ВЫПОЛНЕНО ОКРУГЛЕНИЕ ПО МАТЕМАТИЧЕСКИМ ПРАВИЛАМ

```
# Screen: Взаимодействие с экраном
from pyOpenRPA.Robot import Screen
# Вариант изменения 1-го элемента
# Создать пробную прямоугольную область
lBox = Screen.BoxCreate(inTopInt=10, inLeftInt=10, inHeightInt=10, inWidthInt=10)
# Скорректировать пробную прямоугольную область
lBox2 = Screen.BoxModify(lBox,10,10,"CC"); print(lBox2)
lBox2 = Screen.BoxModify(lBox,10,10,"LU"); print(lBox2)
lBox2 = Screen.BoxModify(lBox,10,10,"LD"); print(lBox2)
lBox2 = Screen.BoxModify(lBox,10,10,"RU"); print(lBox2)
lBox2 = Screen.BoxModify(lBox,10,10,"RD"); print(lBox2)
```


* **Параметры**

    
    * **inBox** (*pyscreeze.Box*) – Экземпляр класса прямоугольной области Box


    * **inDXInt** (*int**, **опциональный*) – Смещение левой верхней координаты по оси X в пикселях (горизонтальная ось).


    * **inDYInt** (*int**, **опциональный*) – Смещение левой верхней координаты по оси Y в пикселях (вертикальная ось).


    * **inPointRuleStr** (*str**, **опциональный*) – Символьное указание точки (подробнее см. выше), относительно которой выполнить изменение прямоугольной области. Допустимые значения: «CC» (по умолчанию), «LU», «LD», «RD», «RU»



* **Результат**

    Экземпляр класса прямоугольной области Box



* **Тип результата**

    pyscreeze.Box



### pyOpenRPA.Robot.Screen.BoxMoveTo(inBox, inDXInt=None, inDYInt=None)
L+,W+: Переместить прямоугольную область (сохранить длину/ширину).

!ВНИМАНИЕ! ПОДДЕРЖИВАЕТ ПАКЕТНУЮ ОБРАТКУ ПРИ ПЕРЕДАЧЕ СПИСКА ЭКЗЕМПЛЯРОВ BOX

```
# Screen: Взаимодействие с экраном
from pyOpenRPA.Robot import Screen
# Вариант изменения 1-го элемента
# Создать пробную прямоугольную область
lBox = Screen.BoxCreate(inTopInt=10, inLeftInt=10, inHeightInt=10, inWidthInt=10)
# Переместить пробную прямоугольную область
lBox = Screen.BoxMoveTo(lBox, inDXInt=100, inDYInt=200)
```


* **Параметры**

    
    * **inBox** (*pyscreeze.Box*) – Экземпляр класса прямоугольной области Box


    * **inDXInt** (*int**, **опциональный*) – Смещение левой верхней координаты по оси X в пикселях (горизонтальная ось).


    * **inDYInt** (*int**, **опциональный*) – Смещение левой верхней координаты по оси Y в пикселях (вертикальная ось).



* **Результат**

    Экземпляр класса прямоугольной области Box



* **Тип результата**

    pyscreeze.Box



### pyOpenRPA.Robot.Screen.BoxOverlay(inBox1, inBox2)
L+,W+:Проверить наложение 2-х прямоугольных областей друг на друга.

```
# Screen: Взаимодействие с экраном
from pyOpenRPA.Robot import Screen
lBox1 = Screen.BoxCreate(inTopInt=10, inLeftInt=10, inHeightInt=100, inWidthInt=1000)
lBox2 = Screen.BoxCreate(inTopInt=160, inLeftInt=160, inHeightInt=100, inWidthInt=100)
Screen.BoxDraw([lBox1, lBox2])
Screen.BoxOverlay(lBox1,lBox2)
```


* **Параметры**

    
    * **inBox1** (*pyscreeze.Box*) – Экземпляр класса прямоугольной области Box


    * **inBox2** (*pyscreeze.Box*) – Экземпляр класса прямоугольной области Box



* **Результат**

    True - inBox1 наложен на inBox2



* **Тип результата**

    bool



### pyOpenRPA.Robot.Screen.ImageClick(inImgPathStr: str, inBoxIndexInt: int = 0, inPointRuleStr: str = 'CC', inIsGrayModeBool: bool = False, inConfidenceFloat: Optional[float] = None, inWaitSecFloat: float = 0, inWaitIntervalSecFloat: float = 0)
L+,W+:Выполнить поиск прямоугольной области по изображению.

!ВНИМАНИЕ! Для использования параметра точности inConfidenceFloat необходим пакет Python opencv-python (python -m pip install opencv-python)

```
# Screen: Взаимодействие с объектами экрана
from pyOpenRPA.Robot import Screen
Screen.ImageClick(inImgPathStr="Button.png",inConfidenceFloat=0.9)
```


* **Параметры**

    
    * **inImgPathStr** (*str**, **относительный или абсолютный*) – Путь к изображению, которое требуется искать на экране


    * **inBoxIndexInt** (*int**, **опционально*) – Индекс прямоугольной области, по которой выполнить клик (если обнаружено несколько областей Box), По умолчанию 0


    * **inPointRuleStr** (*str**, **опциональный*) – Правило идентификации точки на прямоугольной области (правила формирования см. выше). Варианты: «LU»,»CU»,»RU»,»LC»,»CC»,»RC»,»LD»,»CD»,»RD».  По умолчанию «CC»


    * **inIsGrayModeBool** (*bool**, **опционально*) – True - выполнить поиск изображения в режиме серых оттенков (ускоряет производительность, если допускается искажение цвета). По умолчанию False


    * **inConfidenceFloat** (*float**, **опционально*) – Показатель точности. 1.0 - идентичное соответствие, 0.0 - полное несоответствие. По умолчанию 1.0 (None)


    * **inWaitSecFloat** (*float**, **опциональный*) – Время ожидания появления изображения в сек. По умолчанию 0


    * **inWaitIntervalSecFloat** (*float**, **опциональный*) – Интервал повторной проверки наличия изображения. По умолчанию 0



### pyOpenRPA.Robot.Screen.ImageExists(inImgPathStr: str, inIsGrayModeBool: bool = False, inConfidenceFloat: Optional[float] = None)
L+,W+:Проверить, имеется ли на экране хотя бы один подходящий объект. Вернуть булево значение

!ВНИМАНИЕ! Для использования параметра точности inConfidenceFloat необходим пакет Python opencv-python (python -m pip install opencv-python)

```
# Screen: Взаимодействие с объектами экрана
from pyOpenRPA.Robot import Screen
lResult = Screen.ImageExists(inImgPathStr="Button.png",inConfidenceFloat=0.9)
```


* **Параметры**

    
    * **inImgPathStr** (*str**, **относительный или абсолютный*) – Путь к изображению, которое требуется искать на экране


    * **inIsGrayModeBool** (*bool**, **опционально*) – True - выполнить поиск изображения в режиме серых оттенков (ускоряет производительность, если допускается искажение цвета). По умолчанию False


    * **inConfidenceFloat** (*float**, **опционально*) – Показатель точности. 1.0 - идентичное соответствие, 0.0 - полное несоответствие. По умолчанию 1.0 (None)



* **Результат**

    Список из pyscreeze.Box



* **Тип результата**

    list



### pyOpenRPA.Robot.Screen.ImageLocateAll(inImgPathStr: str, inIsGrayModeBool: bool = False, inConfidenceFloat: Optional[float] = None)
L+W+: Искать на экране графические объекты, которые похожи на inImgPathStr. Вернуть список прямоугольных областей на экране (pyscreeze.Box)

!ВНИМАНИЕ! Для использования параметра точности inConfidenceFloat необходим пакет Python opencv-python (python -m pip install opencv-python)

```
# Screen: Взаимодействие с объектами экрана
from pyOpenRPA.Robot import Screen
Screen.ImageLocateAll(inImgPathStr="Button.png",inConfidenceFloat=0.9)
```


* **Параметры**

    
    * **inImgPathStr** (*str**, **относительный или абсолютный*) – Путь к изображению, которое требуется искать на экране


    * **inIsGrayModeBool** (*bool**, **опционально*) – True - выполнить поиск изображения в режиме серых оттенков (ускоряет производительность, если допускается искажение цвета). По умолчанию False


    * **inConfidenceFloat** (*float**, **опционально*) – Показатель точности. 1.0 - идентичное соответствие, 0.0 - полное несоответствие. По умолчанию 1.0 (None)



* **Результат**

    Список из pyscreeze.Box



* **Тип результата**

    list



### pyOpenRPA.Robot.Screen.ImageWaitAppear(inImgPathStr: str, inWaitSecFloat: float = 60, inWaitIntervalSecFloat: float = 1.0, inIsGrayModeBool: bool = False, inConfidenceFloat: Optional[float] = None)
L+,W+:Ожидать появление изображения на протяжении inWaitSecFloat секунд. Проверять с периодичностью inWaitIntervalSecFloat. Вернуть список прямоугольных областей, которые удовлетворяют условию

!ВНИМАНИЕ! Для использования параметра точности inConfidenceFloat необходим пакет Python opencv-python (python -m pip install opencv-python)

```
# Screen: Взаимодействие с объектами экрана
from pyOpenRPA.Robot import Screen
lBoxList = Screen.ImageWaitAppear(inImgPathStr="Button.png",inConfidenceFloat=0.9)
```


* **Параметры**

    
    * **inImgPathStr** (*str**, **относительный или абсолютный*) – Путь к изображению, которое требуется искать на экране


    * **inWaitSecFloat** (*float**, **опциональный*) – Время ожидания появления изображения в сек. По умолчанию IMAGE_WAIT_SEC_FLOAT (60)


    * **inWaitIntervalSecFloat** (*float**, **опциональный*) – Интервал повторной проверки наличия изображения. По умолчанию IMAGE_WAIT_INTERVAL_SEC_FLOAT (1)


    * **inIsGrayModeBool** (*bool**, **опционально*) – True - выполнить поиск изображения в режиме серых оттенков (ускоряет производительность, если допускается искажение цвета). По умолчанию False


    * **inConfidenceFloat** (*float**, **опционально*) – Показатель точности. 1.0 - идентичное соответствие, 0.0 - полное несоответствие. По умолчанию 1.0 (None)



* **Результат**

    Список из pyscreeze.Box или [] если прошло время ожидания.



* **Тип результата**

    list



### pyOpenRPA.Robot.Screen.ImageWaitDisappear(inImgPathStr: str, inWaitSecFloat: float = 60, inWaitIntervalSecFloat: float = 1.0, inIsGrayModeBool: bool = False, inConfidenceFloat: Optional[float] = None)
L+,W+:Ожидать исчезновение изображения на протяжении inWaitSecFloat секунд. Проверять с периодичностью inWaitIntervalSecFloat.

!ВНИМАНИЕ! Для использования параметра точности inConfidenceFloat необходим пакет Python opencv-python (python -m pip install opencv-python)

```
# Screen: Взаимодействие с объектами экрана
from pyOpenRPA.Robot import Screen
Screen.ImageWaitDisappear(inImgPathStr="Button.png",inConfidenceFloat=0.9)
```


* **Параметры**

    
    * **inImgPathStr** (*str**, **относительный или абсолютный*) – Путь к изображению, которое требуется искать на экране


    * **inWaitSecFloat** (*float**, **опциональный*) – Время ожидания появления изображения в сек. По умолчанию IMAGE_WAIT_SEC_FLOAT (60)


    * **inWaitIntervalSecFloat** (*float**, **опциональный*) – Интервал повторной проверки наличия изображения. По умолчанию IMAGE_WAIT_INTERVAL_SEC_FLOAT (1)


    * **inIsGrayModeBool** (*bool**, **опционально*) – True - выполнить поиск изображения в режиме серых оттенков (ускоряет производительность, если допускается искажение цвета). По умолчанию False


    * **inConfidenceFloat** (*float**, **опционально*) – Показатель точности. 1.0 - идентичное соответствие, 0.0 - полное несоответствие. По умолчанию 1.0 (None)



### pyOpenRPA.Robot.Screen.PointClick(inPoint: pyscreeze.Point, inClickCountInt: int = 1, inIntervalSecFloat: float = 0.0, inButtonStr: str = 'left', inMoveDurationSecFloat: float = 0.0, inWaitAfterSecFloat: Optional[float] = None)
L+,W+:Нажатие (вниз) кнопки мыши и затем немедленно выпуск (вверх) её. Допускается следующая параметризация.

```
# Screen: Взаимодействие с мышью объектами экрана
from pyOpenRPA.Robot import Screen
lPoint = Screen.PointCreate(100,150)
Screen.PointClick(lPoint) #Выполнить нажатие левой клавиши мыши
```


* **Параметры**

    
    * **inPoint** (*pyscreeze.Point**, **обязательный*) – Точка на экране, по которой выполнить нажатие мыши


    * **inClickCountInt** (*int**, **опциональный*) – Количество нажатий (вниз и вверх) кнопкой мыши, По умолчанию 1


    * **inIntervalSecFloat** (*float**, **опциональный*) – Интервал ожидания в секундах между нажатиями, По умолчанию 0.0


    * **inButtonStr** (*str**, **опциональный*) – Номер кнопки, которую требуется нажать. Возможные варианты: „left“, „middle“, „right“ или 1, 2, 3. В остальных случаях инициирует исключение ValueError. По умолчанию „left“


    * **inMoveDurationSecFloat** (*float**, **опциональный*) – Время перемещения указателя мыши, По умолчанию 0.0 (моментальное перемещение)


    * **inWaitAfterSecFloat** (*float**, **опциональный*) – Количество секунд, которые ожидать после выполнения операции. По умолчанию установлено в настройках модуля Mouse (базовое значение 0.4)



### pyOpenRPA.Robot.Screen.PointClickDouble(inPoint: pyscreeze.Point, inWaitAfterSecFloat: Optional[float] = None)
L+,W+:Двойное нажатие левой клавиши мыши. Данное действие аналогично вызову функции (см. ниже).

```
# Screen: Взаимодействие с мышью объектами экрана
from pyOpenRPA.Robot import Screen
lPoint = Screen.PointCreate(100,150)
Screen.PointClickDouble(lPoint) #Выполнить двойное нажатие левой клавиши мыши
```


* **Параметры**

    
    * **inPoint** (*pyscreeze.Point**, **обязательный*) – Точка на экране, по которой выполнить нажатие мыши


    * **inWaitAfterSecFloat** (*float**, **опциональный*) – Количество секунд, которые ожидать после выполнения операции. По умолчанию установлено в настройках модуля Mouse (базовое значение 0.4)



### pyOpenRPA.Robot.Screen.PointCreate(inXInt, inYInt)
L+,W+:Создать точку pyscreeze.Point.

```
# Screen: Взаимодействие с мышью объектами экрана
from pyOpenRPA.Robot import Screen
lPoint = Screen.PointCreate(inXInt=10, inYInt=10)
```


* **Параметры**

    
    * **inXInt** (*int**, **опциональный*) – Смещение указателя мыши по оси X (горизонтальная ось).


    * **inYInt** (*int**, **опциональный*) – Смещение указателя мыши по оси Y (вертикальная ось).



* **Результат**

    Точка на экране



* **Тип результата**

    pyscreeze.Point



### pyOpenRPA.Robot.Screen.PointDown(inPoint: pyscreeze.Point, inButtonStr: str = 'left', inWaitAfterSecFloat: Optional[float] = None)
L+,W+:Переместить указатель по координатам inPoint, после чего нажать (вниз) клавишу мыши и не отпускать до выполнения соответсвующей команды (см. Up).

```
# Screen: Взаимодействие с мышью объектами экрана
from pyOpenRPA.Robot import Screen
lPoint = Screen.PointCreate(100,150)
Screen.PointDown(lPoint)
```


* **Параметры**

    
    * **inPoint** (*pyscreeze.Point**, **обязательный*) – Точка на экране, по которой выполнить нажатие мыши


    * **inButtonStr** (*str**, **опциональный*) – Номер кнопки, которую требуется нажать. Возможные варианты: „left“, „middle“, „right“ или 1, 2, 3. В остальных случаях инициирует исключение ValueError. По умолчанию „left“


    * **inWaitAfterSecFloat** (*float**, **опциональный*) – Количество секунд, которые ожидать после выполнения операции. По умолчанию установлено в настройках модуля Mouse (базовое значение 0.4)



### pyOpenRPA.Robot.Screen.PointModify(inPoint, inDXInt, inDYInt)
L+,W+:Скорректировать точку pyscreeze.Point.

```
# Screen: Взаимодействие с мышью объектами экрана
from pyOpenRPA.Robot import Screen
lPoint = Screen.PointCreate(inXInt=10, inYInt=10)
lPoint = Screen.PointModify(inPoint=lPoint, inDXInt=90, inDYInt=10)
```


* **Параметры**

    
    * **inPoint** (*pyscreeze.Point**, **обязательный*) – Точка на экране, по которой выполнить нажатие мыши


    * **inDXInt** (*int**, **опциональный*) – Смещение указателя мыши по оси X (горизонтальная ось).


    * **inDYInt** (*int**, **опциональный*) – Смещение указателя мыши по оси Y (вертикальная ось).



* **Результат**

    Точка на экране



* **Тип результата**

    pyscreeze.Point



### pyOpenRPA.Robot.Screen.PointMoveTo(inPoint: pyscreeze.Point, inWaitAfterSecFloat: Optional[float] = None)
L+,W+:Переместить указатель мыши на позицию inXInt, inYInt за время inMoveDurationSecFloat.

!ВНИМАНИЕ! Отсчет координат inXInt, inYInt начинается с левого верхнего края рабочей области (экрана).

```
# Screen: Взаимодействие с мышью объектами экрана
from pyOpenRPA.Robot import Screen
lPoint = Screen.PointCreate(100,150)
Screen.PointMoveTo(inXInt=100, inYInt=200)
```


* **Параметры**

    
    * **inPoint** (*pyscreeze.Point**, **обязательный*) – Точка на экране, по которой выполнить нажатие мыши


    * **inWaitAfterSecFloat** (*float**, **опциональный*) – Количество секунд, которые ожидать после выполнения операции. По умолчанию установлено в настройках модуля Mouse (базовое значение 0.4)



### pyOpenRPA.Robot.Screen.PointUp(inPoint: pyscreeze.Point, inButtonStr: str = 'left', inWaitAfterSecFloat: Optional[float] = None)
L+,W+:Отпустить (вверх) клавишу мыши.

```
# Screen: Взаимодействие с мышью объектами экрана
from pyOpenRPA.Robot import Screen
lPoint = Screen.PointCreate(100,150)
Screen.PointUp(lPoint)
```


* **Параметры**

    
    * **inPoint** (*pyscreeze.Point**, **обязательный*) – Точка на экране, по которой выполнить нажатие мыши


    * **inButtonStr** (*str**, **опциональный*) – Номер кнопки, которую требуется поднять. Возможные варианты: „left“, „middle“, „right“ или 1, 2, 3. В остальных случаях инициирует исключение ValueError. По умолчанию „left“


    * **inWaitAfterSecFloat** (*float**, **опциональный*) – Количество секунд, которые ожидать после выполнения операции. По умолчанию установлено в настройках модуля Mouse (базовое значение 0.4)


## Быстрая навигация


* [Сообщество pyOpenRPA (telegram)](https://t.me/pyOpenRPA)


* [Сообщество pyOpenRPA (tenchat)](https://tenchat.ru/iMaslov?utm_source=19f2a84f-3268-437f-950c-d987ae42af24)


* [Сообщество pyOpenRPA (вконтакте)](https://vk.com/pyopenrpa)


* [Презентация pyOpenRPA](https://pyopenrpa.ru/Index/pyOpenRPA_product_service.pdf)


* [Портал pyOpenRPA](https://pyopenrpa.ru)


* [Репозиторий pyOpenRPA](https://gitlab.com/UnicodeLabs/OpenRPA)

		.. v1.3.1 replace:: v1.3.1
