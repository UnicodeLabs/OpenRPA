# 1. Первый запуск (Windows & Linux)

Готовы испытать всю мощь перспективных технологий?

Будет очень интересно - начинаем!

## Первый запуск (Windows)

**Для начала необходимо выполнить следующие действия:**


* Скачать репозиторий pyOpenRPA c главной страницы [https://pyopenrpa.ru/](https://pyopenrpa.ru/) (кнопка «Скачать»)


* Распаковать пакет куда угодно!

**ВСЁ - Развертывание pyOpenRPA в Windows завершено! :)**

Также доступен вариант по развертыванию pyOpenRPA в уже существующие проекты Python 3.7+ с помощью команды pip install pyOpenRPA.

Возникли трудности? Рекомендуем обратиться в центр поддержки клиентов pyOpenRPA. Контакты см. здесь: 2. Лицензия & Контакты

## Первый запуск (Linux)

**Для начала необходимо выполнить следующие действия:**

Вариант 1 (быстрый)
- Развернуть !терминальную! чистую ОС linux (например, Ubuntu)
- Переместить папку git://Utils/Configure/ubuntu-kde на linux машину
- Выполнить в терминале: ./configure.sh
- Следовать инструкциям после запуска
- Автоматическая настройка займет около 8 минут при наличии интернета со средней скоростью в 20 мб./сек.

Вариант 2 (длительный)
- Скачать репозиторий pyOpenRPA c главной страницы [https://pyopenrpa.ru/](https://pyopenrpa.ru/) (кнопка «Скачать»)
- Распаковать пакет куда угодно!
- scrot: sudo apt-get scrot (компонент для извлечения скриншотов. pyOpenRPA.Robot.Screen)
- xclip: sudo apt-get install xclip (компонент для работы с буфером обмена. pyOpenRPA.Robot.Clipboard)
- setxkbmap: apt-get install x11-xkb-utils (компонент для взаимодействия с клавиатурой, [https://command-not-found.com/setxkbmap](https://command-not-found.com/setxkbmap))

**ВСЁ - Развертывание pyOpenRPA в Linux завершено! :)**

Также доступен вариант по развертыванию pyOpenRPA в уже существующие проекты Python 3.7+ с помощью команды pip install pyOpenRPA.

Возникли трудности? Рекомендуем обратиться в центр поддержки клиентов pyOpenRPA. Контакты см. здесь: 2. Лицензия & Контакты

## Проверить, что pyOpenRPA развернута корректно? (Windows)

В папке pyOpenRPA запустить интерпретатор Python


* x32 Python (GIT\\Resources\\WPy32-3720\\python-3.7.2\\python.exe)


* x64 Python (GIT\\Resources\\WPy64-3720\\python-3.7.2.amd64\\python.exe)

**Платформа pyOpenRPA успешно развернута корректно. если интерпретаторы python 3.7.2 были запущены без проблем (см. скриншот).**



![image](img/cb5dec8cecafa7d64f6cd14b2672acce.png)

Возникли трудности? Рекомендуем обратиться в центр поддержки клиентов pyOpenRPA. Контакты см. здесь: 2. Лицензия & Контакты

## Проверить, что pyOpenRPA развернута корректно? (Linux)

В папке pyOpenRPA запустить интерпретатор Python


* x64 Python (./GIT/Resources/LPy64-3105/bin/python3.10)


* выполнить вход на страницу оркестратора ([http://localhost:1024](http://localhost:1024)). Логин, установленный по-умолчанию: rpa00 (будет работать в том случае, если при настройке ./configure.sh указывали такую же УЗ). В остальных случаях требуется изменение настроек.

**Платформа pyOpenRPA успешно развернута корректно, если интерпретатор python 3.10 был запущен без проблем.**

Возникли трудности? Рекомендуем обратиться в центр поддержки клиентов pyOpenRPA. Контакты см. здесь: 2. Лицензия & Контакты

## Быстрая навигация


* [Сообщество pyOpenRPA (telegram)](https://t.me/pyOpenRPA)


* [Сообщество pyOpenRPA (tenchat)](https://tenchat.ru/iMaslov?utm_source=19f2a84f-3268-437f-950c-d987ae42af24)


* [Сообщество pyOpenRPA (вконтакте)](https://vk.com/pyopenrpa)


* [Презентация pyOpenRPA](https://pyopenrpa.ru/Index/pyOpenRPA_product_service.pdf)


* [Портал pyOpenRPA](https://pyopenrpa.ru)


* [Репозиторий pyOpenRPA](https://gitlab.com/UnicodeLabs/OpenRPA)

		.. v1.3.1 replace:: v1.3.1
