# 5. Права доступа пользователей UAC

## Описание

Оркестратор обладает уникальным и гибким механизмом контроля и распределения прав доступа пользователей (UAC - User Access Control). Концепция pyOpenRPA заключается в том, что настройка доступа должна быть максимально функциональной, но при этом удобной в использовании.

Если требуется дать все права доступа - не нужно ничего редактировать - они будут по умолчанию.

Если нужно внести корректировки - вы можете указать лишь те ключи доступа, которые потребуются пользователю.

Также UAC механизм распределения прав доступа отлично интегрируется со сторонними системами безопасности, которые уже используются в компаниях, например Active Directory и другие.

Функции по работе с UAC представлены в группе `Orchestrator.UAC` в разделе описания функций: 2. Функции.

Если у вас останутся вопросы, то вы всегда можете обратиться в центр поддержки клиентов pyOpenRPA. Контакты: 2. Лицензия & Контакты

pyOpenRPA - роботы помогут!

## UAC Dict for Orchestrator WEB UI rights

UAC Dict for pyOpenRPA Orchestrator WEB UI rights.

```
"pyOpenRPADict":{
    "CPKeyDict":{ # Empty dict - all access
        # "CPKeyStr"{
        # }
    },
    "RDPKeyDict":{ # Empty dict - all access
        #"RDPKeyStr"{
        #   "FullscreenBool": True,
        #   "IgnoreBool":True,
        #   "ReconnectBool": True
        #   "NothingBool": True # USe option if you dont want to give some access to the RDP controls
        # }
    },
    "AgentKeyDict": { # Empty dict - all access
        # "AgentKeyStr"{
        # }
    },
    "AdminDict":{ # Empty dict - all access
        "LogViewerBool":True, # Show log viewer on the web page
        "CMDInputBool":True, # Execute CMD on the server side and result to the logs
        "ScreenshotViewerBool":True, # Show button to look screenshots
        "RestartOrchestratorBool": True, # Restart orchestrator activity
        "RestartOrchestratorGITPullBool": True, # Turn off (RDP remember) orc + git pull + Turn on (rdp remember)
        "RestartPCBool": True, # Send CMD to restart pc
        "NothingBool":True # USe option if you dont want to give some access to the RDP controls
    },
    "ActivityDict": { # Empty dict - all access
        "ActivityListExecuteBool": True,  # Execute activity at the current thread
        "ActivityListAppendProcessorQueueBool": True  # Append activity to the processor queue
    }
}
```

## Быстрая навигация


* [Сообщество pyOpenRPA (telegram)](https://t.me/pyOpenRPA)


* [Сообщество pyOpenRPA (tenchat)](https://tenchat.ru/iMaslov?utm_source=19f2a84f-3268-437f-950c-d987ae42af24)


* [Сообщество pyOpenRPA (вконтакте)](https://vk.com/pyopenrpa)


* [Презентация pyOpenRPA](https://pyopenrpa.ru/Index/pyOpenRPA_product_service.pdf)


* [Портал pyOpenRPA](https://pyopenrpa.ru)


* [Репозиторий pyOpenRPA](https://gitlab.com/UnicodeLabs/OpenRPA)

		.. v1.3.1 replace:: v1.3.1
