import os
#Add chrome webdriver to PATH system enviroment (need to selenium)
os.environ["PATH"]=os.environ["PATH"]+";C:\\Abs\\Archive\\scopeSrcUL\\OpenRPA\\Resources\\SeleniumWebDrivers\\Chrome\\chromedriver_win32 v80.0.3987.16\\"


from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys

driver = webdriver.Chrome()

#Open URL
driver.get("https://www.yandex.ru")

#elem = driver.find_element_by_css_selector("input.input__control.input__input")
wait = WebDriverWait(driver, 10)
element = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input.input__control.input__input")))
#Type text in search imput
element.send_keys("Test request")

#Submit the search
element.send_keys(Keys.RETURN)
