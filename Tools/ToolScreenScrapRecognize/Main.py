import psutil
import datetime
import logging
import os
import tkinter as tk

from desktopmagic.screengrab_win32 import (
getDisplayRects, saveScreenToBmp, saveRectToBmp, getScreenAsImage,
getRectAsImage, getDisplaysAsImages)

#pytesseract
try:
    from PIL import Image
except ImportError:
    import Image
import pytesseract
pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'
# Example tesseract_cmd = r'C:\Program Files (x86)\Tesseract-OCR\tesseract'

class App(tk.Frame):
    def __init__( self, parent):
        tk.Frame.__init__(self, parent)
        self._createVariables(parent)
        self._createCanvas()
        self._createCanvasBinding()

    def _createVariables(self, parent):
        self.parent = parent
        self.rectx0 = 0
        self.recty0 = 0
        self.rectx1 = 0
        self.recty1 = 0
        self.rectid = None

    def _createCanvas(self):
        self.canvas = tk.Canvas(self.parent, width = self.winfo_screenwidth(), height = self.winfo_screenheight(),
                                bg = "white" )
        self.canvas.grid(row=0, column=0, sticky='nsew')

    def _createCanvasBinding(self):
        self.canvas.bind( "<Button-1>", self.startRect )
        self.canvas.bind( "<ButtonRelease-1>", self.stopRect )
        self.canvas.bind( "<B1-Motion>", self.movingRect )
        
    def startRect(self, event):
        self.canvas.delete("all")
        #Translate mouse screen x0,y0 coordinates to canvas coordinates
        self.rectx0 = self.canvas.canvasx(event.x)
        self.recty0 = self.canvas.canvasy(event.y) 
        #Create rectangle
        self.rectid = self.canvas.create_rectangle(
            self.rectx0, self.recty0, self.rectx0, self.recty0, fill="#4eccde")
        print('Rectangle {0} started at {1} {2} {3} {4} '.
              format(self.rectid, self.rectx0, self.recty0, self.rectx0,
                     self.recty0))

    def movingRect(self, event):
        #Translate mouse screen x1,y1 coordinates to canvas coordinates
        self.rectx1 = self.canvas.canvasx(event.x)
        self.recty1 = self.canvas.canvasy(event.y)
        #Modify rectangle x1, y1 coordinates
        self.canvas.coords(self.rectid, self.rectx0, self.recty0,
                      self.rectx1, self.recty1)
        #print('Rectangle x1, y1 = ', self.rectx1, self.recty1)

    def stopRect(self, event):
        #Translate mouse screen x1,y1 coordinates to canvas coordinates
        self.rectx1 = self.canvas.canvasx(event.x)
        self.recty1 = self.canvas.canvasy(event.y)
        self.canvas.delete("all")
        self.parent.attributes('-alpha', 0.0)
        #Save rect in file png
        
        rect256 = getRectAsImage(
            (
                int(self.rectx0 if self.rectx0 <= self.rectx1 else self.rectx1), 
                int(self.recty0 if self.recty0 <= self.recty1 else self.recty1), 
                int(self.rectx1 if self.rectx0 <= self.rectx1 else self.rectx0), 
                int(self.recty1 if self.recty0 <= self.recty1 else self.recty0)
            )
        )
        rect256.save('screencapture_256_256.png', format='png')
        self.parent.attributes('-alpha', 0.2)
        #Modify rectangle x1, y1 coordinates
        self.canvas.coords(self.rectid, self.rectx0, self.recty0,
                      self.rectx1, self.recty1)
        print('Rectangle ended')
        print('##################Recognition result###########################')
        print(pytesseract.image_to_string(Image.open('screencapture_256_256.png'), lang='rus+eng'))
        # Get verbose data including boxes, confidences, line and page numbers
        print('##Get verbose data including boxes, confidences, line and page numbers##')
        print(pytesseract.image_to_data(Image.open('screencapture_256_256.png'), lang='rus+eng'))
        # Get information about orientation and script detection
        #print('##Get information about orientation and script detection##')
        #print(pytesseract.image_to_osd(Image.open('screencapture_256_256.png'), lang='rus+eng'))

if __name__ == "__main__":
    root = tk.Tk()
    def quit(l):
        root.destroy()
    root.bind("<Escape>",quit)
    root.attributes('-alpha', 0.2)
    root.attributes("-fullscreen", True)
    #pad=3
    #root.geometry("{0}x{1}+0+0".format(
    #        root.winfo_screenwidth()-pad, root.winfo_screenheight()-pad))
    print(f"{root.winfo_screenwidth()}x{root.winfo_screenheight()}")
    root.geometry( f"{root.winfo_screenwidth()}x{root.winfo_screenheight()}" )
    app = App(root)
    root.mainloop()