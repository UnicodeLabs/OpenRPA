#!/bin/bash

#ВНИМАНИЕ! ЗАПУСКАТЬ ИЗ ПОД ROOT

echo "Добро пожаловать в мастер добавления УЗ робота pyOpenRPA (ubuntu + kde plasma)"
echo "!ВНИМАНИЕ! Убедитесь в том, что скрипт запущен из под root (с правами администратора)"
echo "!ВНИМАНИЕ! Добавление УЗ роботов делать только после настройки УЗ оркестратора (см. configure.sh)"
read -p "Установите логин УЗ робота [например: rpa01]: " VAR_USER
VAR_USER=${VAR_USER:-rpa01}
read -p "Установите группу роботов на машине [по-умолчанию: rpa]: " VAR_ROBOTGROUP
VAR_ROBOTGROUP=${VAR_ROBOTGROUP:-rpa}
echo "Установите пароль [по-умолчанию: 123QWEasd]" 
read -s VAR_PASSWORD
VAR_PASSWORD=${VAR_PASSWORD:-123QWEasd}
read -p "Установите номер терминала tty для УЗ оркестратора [по-умолчанию: 3]: " VAR_TTY
VAR_TTY_NUM=${VAR_TTY:-3}
VAR_TTY=tty${VAR_TTY:-3}
echo "Подключиться к УЗ робота после автоматической настройки можно будет через протокол VNC. Параметры для подключения: ip адрес виртуальной машины, порт (укажите ниже), пароль (от УЗ робота)"
read -p "Укажите номер порта для VNC [по-умолчанию: 5901]: " VAR_VNC_PORT
VAR_VNC_PORT=${VAR_VNC_PORT:-5901}

# УЗ РОБОТА [TESTED+]
useradd -b /home -d /home/$VAR_USER -g $VAR_ROBOTGROUP -G ssh,tty,audio,video,mail -p $(openssl passwd -crypt $VAR_PASSWORD) -s /bin/bash $VAR_USER
mkhomedir_helper $VAR_USER
rm -rf /etc/systemd/system/getty@$VAR_TTY.service.d
mkdir -p /etc/systemd/system/getty@$VAR_TTY.service.d
cd /home/$VAR_USER
# alternative systemctl edit getty@$VAR_TTY
echo "\
[Service]
ExecStart=
ExecStart=-/sbin/agetty --autologin $VAR_USER --noclear %I 38400 linux
" >> /etc/systemd/system/getty@$VAR_TTY.service.d/override.conf
systemctl enable getty@$VAR_TTY.service

# СЕРВЕР X11VNC
rm -rf /home/$VAR_USER/x11vnc
mkdir -p /home/$VAR_USER/x11vnc
x11vnc -storepasswd "$VAR_PASSWORD" /home/$VAR_USER/x11vnc/pass
echo "\
#!/bin/bash
x11vnc -noxdamage -shared -dontdisconnect -many -noxfixes -rfbauth /home/$VAR_USER/x11vnc/pass -rfbport $VAR_VNC_PORT
" >> /home/$VAR_USER/x11vnc/start.sh

#СОЗДАТЬ XORG.CONF - ВИРТУАЛЬНЫЙ ВИДЕОДРАЙВЕР [TESTED+]
rm -rf /home/$VAR_USER/xorg.conf
echo "\
Section \"Monitor\"
        Identifier \"dummy_monitor\"
        HorizSync 28.0-80.0
        VertRefresh 48.0-75.0
        Modeline \"1920x1080\" 172.80 1920 2040 2248 2576 1080 1081 1084 1118
EndSection

Section \"Device\"
        Identifier \"dummy_card\"
        VideoRam 256000
        Driver \"dummy\"
EndSection

Section \"Screen\"
        Identifier \"dummy_screen\"
        Device \"dummy_card\"
        Monitor \"dummy_monitor\"
        SubSection \"Display\"
        EndSubSection
EndSection
" >> /home/$VAR_USER/xorg.conf
chown $VAR_USER:$VAR_ROBOTGROUP /home/$VAR_USER/xorg.conf

#СОЗДАТЬ .bash_profile для запуска графической подсистемы [TESTED+]
rm -rf /home/$VAR_USER/.bash_profile
echo "\
export LANG=C.UTF-8
export XORGCONFIG=/home/$VAR_USER/xorg.conf
sleep 3s
startx -- :$VAR_TTY_NUM
" >> /home/$VAR_USER/.bash_profile
chown $VAR_USER:$VAR_ROBOTGROUP /home/$VAR_USER/.bash_profile

#ДОБАВИТЬ В АВТОЗАГРУЗКУ KDE AGENT [TESTED+]
rm -rf /home/$VAR_USER/.config/autostart/orpa-agent-start.sh.desktop
mkdir -p /home/$VAR_USER/.config/autostart
echo "\
[Desktop Entry]
Comment[en_US]=
Comment=
Exec=/opt/rpa/OpenRPA-prd/Agent/start.sh
GenericName[en_US]=
GenericName=
Icon=system-run
MimeType=
Name[en_US]=
Name=
OnlyShowIn=KDE;
Path=/opt/rpa/OpenRPA-prd/Agent
StartupNotify=true
Terminal=true
TerminalOptions=
Type=Application
X-DBUS-ServiceName=
X-DBUS-StartupType=none
X-KDE-SubstituteUID=false
X-KDE-Username=
" >> /home/$VAR_USER/.config/autostart/orpa-agent-start.sh.desktop
chown $VAR_USER:$VAR_ROBOTGROUP /home/$VAR_USER/.config/autostart/orpa-agent-start.sh.desktop

#ДОБАВИТЬ В АВТОЗАГРУЗКУ KDE X11VNC
rm -rf /home/$VAR_USER/.config/autostart/x11vnc-start.sh.desktop
mkdir -p /home/$VAR_USER/.config/autostart
echo "\
[Desktop Entry]
Comment[en_US]=
Comment=
Exec=/home/$VAR_USER/x11vnc/start.sh
GenericName[en_US]=
GenericName=
Icon=system-run
MimeType=
Name[en_US]=
Name=
OnlyShowIn=KDE;
Path=/home/$VAR_USER/x11vnc
StartupNotify=true
Terminal=false
TerminalOptions=
Type=Application
X-DBUS-ServiceName=
X-DBUS-StartupType=none
X-KDE-SubstituteUID=false
X-KDE-Username=
" >> /home/$VAR_USER/.config/autostart/x11vnc-start.sh.desktop
chown $VAR_USER:$VAR_ROBOTGROUP /home/$VAR_USER/.config/autostart/x11vnc-start.sh.desktop

#ОТКЛЮЧИТЬ LOCKSCREEN KDE [TESTED+]
rm -rf /home/$VAR_USER/.config/kscreenlockerrc
mkdir -p /home/$VAR_USER/.config
echo "\
[$Version]
update_info=kscreenlocker.upd:0.1-autolock

[Daemon]
Autolock=false
LockOnResume=false
" >> /home/$VAR_USER/.config/kscreenlockerrc
chown $VAR_USER:$VAR_ROBOTGROUP /home/$VAR_USER/.config/kscreenlockerrc

#Рекурсивная установка группы RPA для всех каталогов внутри /opt/rpa [TESTED+]
cd ..
chown -R root:$VAR_ROBOTGROUP /opt/rpa
chmod -R 775 /opt/rpa

## ПРАВА ДОСТУПА ДОМАШНЕГО КАТАЛОГА
chown -R $VAR_USER:$VAR_ROBOTGROUP /home/$VAR_USER
chmod -R 755 /home/$VAR_USER
## ПРАВА ДОСТУПА ВНУТРИ ДОМАШНЕГО КАТАЛОГА К X11VNC
chmod ugo+r /home/$VAR_USER/x11vnc/pass
chmod ug+x /home/$VAR_USER/x11vnc/start.sh


