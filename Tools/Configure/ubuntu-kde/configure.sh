#!/bin/bash

#ВНИМАНИЕ! ЗАПУСКАТЬ ИЗ ПОД ROOT

echo "Добро пожаловать в мастер настройки pyOpenRPA (ubuntu + kde plasma)"
echo "!ВНИМАНИЕ! Убедитесь в том, что скрипт запущен из под root (с правами администратора)"
read -p "Установите логин УЗ оркестратора [по-умолчанию: rpa00]: " VAR_USER
VAR_USER=${VAR_USER:-rpa00}
read -p "Установите группу роботов на машине [по-умолчанию: rpa]: " VAR_ROBOTGROUP
VAR_ROBOTGROUP=${VAR_ROBOTGROUP:-rpa}
echo "Установите пароль [по-умолчанию: 123QWEasd]" 
read -s VAR_PASSWORD
VAR_PASSWORD=${VAR_PASSWORD:-123QWEasd}
read -p "Установите номер терминала tty для УЗ оркестратора [по-умолчанию: 2]: " VAR_TTY
VAR_TTY=tty${VAR_TTY:-2}
echo "Подключиться к УЗ оркестратора после автоматической настройки можно будет через протокол VNC. Параметры для подключения: ip адрес виртуальной машины, порт (укажите ниже), пароль (от УЗ оркестратора)"
read -p "Укажите номер порта для VNC [по-умолчанию: 5900]: " VAR_VNC_PORT
VAR_VNC_PORT=${VAR_VNC_PORT:-5900}

# Установка компонентов [TESTED+]
apt update
apt upgrade
apt  -y install aptitude
aptitude install kde-plasma-desktop
apt -y install x11vnc #VNC сервер для подключения к УЗ роботов/оркестратора
apt -y install krdc #Клиент для подключения к VNC/ RDP
apt -y install remmina # rdp client для оркестратора
apt -y install xserver-xorg-video-dummy # драйвер для создания виртуальных графических сессий
apt -y install ufw # фаервол
apt -y install wget # скачивальщик
apt -y install xinit # X сервер
apt -y install scrot # screenshots
apt -y install xclip # буфер обмена
apt -y install openssl # шифрование
apt -y install x11-xkb-utils # переключение раскладки
apt -y install git

# ПЕРЕНОС НАСТРОЕК SSH [TESTED+]
rm -rf /etc/ssh/sshd_config
mkdir -p /etc/ssh
cp sshd_config /etc/ssh/sshd_config
chmod -R 644 "/etc/ssh/sshd_config"
chown -R root:root /etc/ssh/sshd_config

# НАСТРОЙКА ОКРУЖЕНИЯ [TESTED+]
cd /opt
groupadd $VAR_ROBOTGROUP
mkdir rpa
chown -R root:$VAR_ROBOTGROUP rpa
chmod -R 770 rpa


# Скомпилировать исправленную версию x11vnc [TESTED+]
cd /opt/rpa
apt -y install gcc
apt -y install make
apt -y install -y libxtst-dev libssl-dev libjpeg-dev
wget http://x11vnc.sourceforge.net/dev/x11vnc-0.9.14-dev.tar.gz
gzip -dc x11vnc-0.9.14-dev.tar.gz | tar -xvf -
cd x11vnc-0.9.14/
./configure --prefix=/usr/local CFLAGS='-g -O2 -fno-stack-protector -Wall'
make
make install
cd ..
rm -rf x11vnc-0.9.14-dev.tar.gz


# Скачать pyOpenRPA [TESTED+]
cd /opt/rpa
wget https://gitlab.com/UnicodeLabs/OpenRPA/-/archive/prd/OpenRPA-prd.tar
tar -xvf OpenRPA-prd.tar
chmod ug+x "/opt/rpa/OpenRPA-prd/Resources/LChrome64-10305060114/data/chrome"
chmod ug+x "/opt/rpa/OpenRPA-prd/Resources/LChrome64-10305060114/data/chrome_crashpad_handler"
chmod ug+x "/opt/rpa/OpenRPA-prd/Resources/LPy64-3105/bin/python3.10"
chmod ug+x "/opt/rpa/OpenRPA-prd/Resources/SeleniumWebDrivers/Chrome/chromedriver_lin64 v103.0.5060.53/chromedriver"
chmod ug+x "/opt/rpa/OpenRPA-prd/Orchestrator/start.sh"
chmod ug+x "/opt/rpa/OpenRPA-prd/Agent/start.sh"
chmod ug+x "/opt/rpa/OpenRPA-prd/Utils/Jupyter-notebooks/start.sh"
rm OpenRPA-prd.tar

#НЕ ГРУЗИТЬ ГРАФИЧЕСКИЙ МЕНЕДЖЕР ЛОГИНА (ДЛЯ РОБОТА ОН БЕСПОЛЕЗЕН) [TESTED+]
systemctl enable multi-user.target --force
systemctl set-default multi-user.target

# УЗ ОРКЕСТРАТОРА [TESTED+]
useradd -b /home -d /home/$VAR_USER -g $VAR_ROBOTGROUP -G ssh,tty,audio,video,mail -p $(openssl passwd -crypt $VAR_PASSWORD) -s /bin/bash $VAR_USER
mkhomedir_helper $VAR_USER
rm -rf /etc/systemd/system/getty@$VAR_TTY.service.d
mkdir -p /etc/systemd/system/getty@$VAR_TTY.service.d
cd /home/$VAR_USER
# alternative systemctl edit getty@$VAR_TTY
echo "\
[Service]
ExecStart=
ExecStart=-/sbin/agetty --autologin $VAR_USER --noclear %I 38400 linux
" >> /etc/systemd/system/getty@$VAR_TTY.service.d/override.conf
systemctl enable getty@$VAR_TTY.service

# СЕРВЕР X11VNC
rm -rf /home/$VAR_USER/x11vnc
mkdir -p /home/$VAR_USER/x11vnc
x11vnc -storepasswd "$VAR_PASSWORD" /home/$VAR_USER/x11vnc/pass
echo "\
#!/bin/bash
x11vnc -noxdamage -shared -dontdisconnect -many -noxfixes -rfbauth /home/$VAR_USER/x11vnc/pass -rfbport $VAR_VNC_PORT
" >> /home/$VAR_USER/x11vnc/start.sh

#СОЗДАТЬ XORG.CONF - ВИРТУАЛЬНЫЙ ВИДЕОДРАЙВЕР [TESTED+]
rm -rf /home/$VAR_USER/xorg.conf
echo "\
Section \"Monitor\"
        Identifier \"dummy_monitor\"
        HorizSync 28.0-80.0
        VertRefresh 48.0-75.0
        Modeline \"1920x1080\" 172.80 1920 2040 2248 2576 1080 1081 1084 1118
EndSection

Section \"Device\"
        Identifier \"dummy_card\"
        VideoRam 256000
        Driver \"dummy\"
EndSection

Section \"Screen\"
        Identifier \"dummy_screen\"
        Device \"dummy_card\"
        Monitor \"dummy_monitor\"
        SubSection \"Display\"
        EndSubSection
EndSection
" >> /home/$VAR_USER/xorg.conf
chown $VAR_USER:$VAR_ROBOTGROUP /home/$VAR_USER/xorg.conf

#СОЗДАТЬ .bash_profile для запуска графической подсистемы [TESTED+]
rm -rf /home/$VAR_USER/.bash_profile
echo "\
export LANG=C.UTF-8
export XORGCONFIG=/home/$VAR_USER/xorg.conf
sleep 3s
startx
" >> /home/$VAR_USER/.bash_profile
chown $VAR_USER:$VAR_ROBOTGROUP /home/$VAR_USER/.bash_profile

#ДОБАВИТЬ В АВТОЗАГРУЗКУ KDE ОРКЕСТРАТОР [TESTED+]
rm -rf /home/$VAR_USER/.config/autostart/orpa-orc-start.sh.desktop
mkdir -p /home/$VAR_USER/.config/autostart
echo "\
[Desktop Entry]
Comment[en_US]=
Comment=
Exec=/opt/rpa/OpenRPA-prd/Orchestrator/start.sh
GenericName[en_US]=
GenericName=
Icon=system-run
MimeType=
Name[en_US]=
Name=
OnlyShowIn=KDE;
Path=/opt/rpa/OpenRPA-prd/Orchestrator
StartupNotify=true
Terminal=true
TerminalOptions=
Type=Application
X-DBUS-ServiceName=
X-DBUS-StartupType=none
X-KDE-SubstituteUID=false
X-KDE-Username=
" >> /home/$VAR_USER/.config/autostart/orpa-orc-start.sh.desktop
chown $VAR_USER:$VAR_ROBOTGROUP /home/$VAR_USER/.config/autostart/orpa-orc-start.sh.desktop

#ДОБАВИТЬ В АВТОЗАГРУЗКУ KDE X11VNC
rm -rf /home/$VAR_USER/.config/autostart/x11vnc-start.sh.desktop
mkdir -p /home/$VAR_USER/.config/autostart
echo "\
[Desktop Entry]
Comment[en_US]=
Comment=
Exec=/home/$VAR_USER/x11vnc/start.sh
GenericName[en_US]=
GenericName=
Icon=system-run
MimeType=
Name[en_US]=
Name=
OnlyShowIn=KDE;
Path=/home/$VAR_USER/x11vnc
StartupNotify=true
Terminal=false
TerminalOptions=
Type=Application
X-DBUS-ServiceName=
X-DBUS-StartupType=none
X-KDE-SubstituteUID=false
X-KDE-Username=
" >> /home/$VAR_USER/.config/autostart/x11vnc-start.sh.desktop
chown $VAR_USER:$VAR_ROBOTGROUP /home/$VAR_USER/.config/autostart/x11vnc-start.sh.desktop

#ОТКЛЮЧИТЬ LOCKSCREEN KDE [TESTED+]
rm -rf /home/$VAR_USER/.config/kscreenlockerrc
mkdir -p /home/$VAR_USER/.config
echo "\
[$Version]
update_info=kscreenlocker.upd:0.1-autolock

[Daemon]
Autolock=false
LockOnResume=false
" >> /home/$VAR_USER/.config/kscreenlockerrc
chown $VAR_USER:$VAR_ROBOTGROUP /home/$VAR_USER/.config/kscreenlockerrc

#Рекурсивная установка группы RPA для всех каталогов внутри /opt/rpa [TESTED+]
cd ..
chown -R root:$VAR_ROBOTGROUP /opt/rpa
chmod -R 775 /opt/rpa

## ПРАВА ДОСТУПА ДОМАШНЕГО КАТАЛОГА
chown -R $VAR_USER:$VAR_ROBOTGROUP /home/$VAR_USER
chmod -R 755 /home/$VAR_USER
## ПРАВА ДОСТУПА ВНУТРИ ДОМАШНЕГО КАТАЛОГА К X11VNC
chmod ugo+r /home/$VAR_USER/x11vnc/pass
chmod ug+x /home/$VAR_USER/x11vnc/start.sh

reboot -f

