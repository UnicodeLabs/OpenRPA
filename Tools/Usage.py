###############################
#Technology GUI
###############################
#Init UIDesktop
from pyOpenRPA.Robot import UIDesktop
#Init the robot #Optional if has 2 bitness
lRobotConfig={
    "Python32FullPath": None, #Set from user: "..\\Resources\\WPy32-3720\\python-3.7.2\\OpenRPARobotGUIx32.exe"
    "Python64FullPath": None, #Set from user
    "Python32ProcessName": "OpenRPAUIDesktopX32.exe", #Config set once
    "Python64ProcessName": "OpenRPAUIDesktopX64.exe" #Config set once
}
RobotConnector.UIDesktop.Utils.ProcessBitness.SettingsInit(lRobotConfig)

#Selector: Folder list (framework uia)
lGUISelectorFolderList = [
		{"class_name":"CabinetWClass","backend":"uia"},
		{"ctrl_index":2},
		{"ctrl_index":0},
		{"ctrl_index":2},
		{"ctrl_index":0}
	]
	
#Highlight the list in folder app (uia framework)
UIDesktop.UIOSelector_Get_UIO(lGUISelectorFolderList).draw_outline()

#Print the children list
print(UIDesktop.UIOSelector_Get_UIO(lGUISelectorFolderList).children())

#Right click
UIDesktop.UIOSelector_Get_UIO(lGUISelectorFolderList).right_click_input()

###############################
#Technology Selenium
###############################
#Init the selenium driver
#https://selenium-python.readthedocs.io/
import os
#Add chrome webdriver to PATH system enviroment (need to selenium)
os.environ["PATH"]=os.environ["PATH"]+";C:\\Abs\\Archive\\scopeSrcUL\\OpenRPA\\Resources\\SeleniumWebDrivers\\Chrome\\chromedriver_win32 v80.0.3987.16\\"
#Import selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
driver = webdriver.Chrome()

#Open URL
driver.get("http://www.python.org")

#Find the search input
elem = driver.find_element_by_name("q")

#Type text in search imput
elem.send_keys("pycon")

#Submit the search
elem.send_keys(Keys.RETURN)

#Close the driver
driver.close()
#################################
import os
#Add chrome webdriver to PATH system enviroment (need to selenium)
os.environ["PATH"]=os.environ["PATH"]+";C:\\Abs\\Archive\\scopeSrcUL\\OpenRPA\\Resources\\SeleniumWebDrivers\\Chrome\\chromedriver_win32 v80.0.3987.16\\"


from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys

driver = webdriver.Chrome()

#Open URL
driver.get("https://www.yandex.ru")

#elem = driver.find_element_by_css_selector("input.input__control.input__input")
wait = WebDriverWait(driver, 10)
element = wait.until(EC.element_to_be_clickable((By.CSS, "input.input__control.input__input")))
#Type text in search imput
element.send_keys("Test request")

#Submit the search
element.send_keys(Keys.RETURN)

#Close the driver
#driver.close()

###############################
#Technology Image & Mouse
###############################
#https://pypi.org/project/PyAutoGUI/
import pyautogui
lScreenPath = "Path\\to\\image.png"
#Search image on screen
buttonx, buttony = pyautogui.locateCenterOnScreen(lScreenPath) # returns (x, y) of matching region
#Moucse click on X:buttonx Y:buttony
pyautogui.click(buttonx, buttony)

###############################
#Technology Keyboard & Clipboard
###############################
#https://pypi.org/project/keyboard/
import keyboard

#Write a text (selected language dont affect)
keyboard.write('The quick brown fox jumps over the lazy dog.')

# Blocks until you press esc.
keyboard.wait('esc')

# Record events until 'esc' is pressed.
recorded = keyboard.record(until='esc')
# Then replay back at three times the speed.
keyboard.play(recorded, speed_factor=3)

#Add hotkey
keyboard.add_hotkey('ctrl+shift+a', print, args=('triggered', 'hotkey'))

###############################
#Technology Message boxes
###############################
#https://pypi.org/project/PyAutoGUI/
import pyautogui
#Show alert message box
pyautogui.alert('This is an alert box.')

#Ask a question (Yes/No)
pyautogui.confirm('Shall I proceed?')

#Ask a question with answer options
pyautogui.confirm('Enter option.', buttons=['A', 'B', 'C'])

#Ask a question with text answer
pyautogui.prompt('What is your name?')

#Ask a password (password will be hidden)
pyautogui.password('Enter password (text will be hidden)')