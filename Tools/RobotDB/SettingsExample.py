import psutil
import datetime
import logging
import os
import pdb
#RobotDB settings
def Settings():
    import os
    import pyOpenRPA.Orchestrator
    lOrchestratorFolder = "\\".join(pyOpenRPA.Orchestrator.__file__.split("\\")[:-1])
    mDict = {
        "SQLite":{
            "DBPath":os.path.join("\\".join(__file__.split("\\")[:-1]),"DB.db")
        },
        "Server": {
            "ListenPort_": "Порт, по которому можно подключиться к демону",
            "ListenPort": 8081,
            "ListenURLList": [
                {
                    "Description": "Local machine test",
                    "URL_": "Сетевое расположение сервера демона",
                    "URL": "http://127.0.0.1:8081"
                }
            ],
            "AccessUsers": { #Default - all URL is blocked
                "FlagCredentialsAsk": False, #Turn on Authentication
                "RuleDomainUserDict": {
                    #("DOMAIN", "USER"): { !!!!!only in upper case!!!!
                    #    "MethodMatchURLBeforeList": [
                    #       {
                    #           "Method":"GET|POST",
                    #           "MatchType":"BeginWith|Contains|Equal|EqualCase",
                    #           "URL":"",
                    #           "FlagAccessDefRequestGlobalAuthenticate": None, #Return bool
                    #           "FlagAccess": True       
                    #        }
                    #    ]
                    #}
                },
                "RuleMethodMatchURLBeforeList": [ #General MethodMatchURL list (no domain/user)
                    #       {
                    #           "Method":"GET|POST",
                    #           "MatchType":"BeginWith|Contains|Equal|EqualCase",
                    #           "URL":"",
                    #           "FlagAccessDefRequestGlobalAuthenticate": None, #Return bool
                    #           "FlagAccess": True       
                    #        }
                ],
                "AuthTokensDict": {
                    #"<AuthToken>":{"User":"", "Domain":"", "TokenDatetime":<Datetime>, "FlagDoNotExpire":True}
                }
            },
            "URLList":[ #List of available URLs with the orchestrator server
                #{
                #    "Method":"GET|POST",
                #    "URL": "/index", #URL of the request
                #    "MatchType": "", #"BeginWith|Contains|Equal|EqualCase",
                #    "ResponseFilePath": "", #Absolute or relative path
                #    "ResponseFolderPath": "", #Absolute or relative path
                #    "ResponseContentType": "", #HTTP Content-type
                #    "ResponseDefRequestGlobal": None #Function with str result
                #}
                ]
        },
        "Logger": logging.getLogger("RobotDB"),
        "Storage": {} #Orchestrator storage - not used
    }
    #Создать файл логирования
    # add filemode="w" to overwrite
    if not os.path.exists("Reports"):
        os.makedirs("Reports")
    ##########################
    #Подготовка логгера Robot
    #########################
    mRobotLogger=mDict["Logger"]
    mRobotLogger.setLevel(logging.INFO)
    # create the logging file handler
    mRobotLoggerFH = logging.FileHandler("Reports\ReportRobotDB_"+datetime.datetime.now().strftime("%Y_%m_%d")+".log")
    mRobotLoggerFormatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    mRobotLoggerFH.setFormatter(mRobotLoggerFormatter)
    # add handler to logger object
    mRobotLogger.addHandler(mRobotLoggerFH)
    ############################################
    ###################################
    #Init .py files from Settings folder
    ####################################
    #Get file list from Settings folder
    import os
    import pdb
    #lFunction to call in subfiles
    lSubmoduleFunctionName = "SettingsUpdate"
    #pdb.set_trace()
    #lSettingsPath = os.path.join(inSettingsFolderPath, "Settings")
    lSettingsPath = "\\".join(os.path.join(os.getcwd(),__file__).split("\\")[:-1])
    #lSettingsPath = os.path.join(os.getcwd(), "Settings")
    #Lambda function to get files .py from settings folder except Settings.py
    lFileList = [f for f in os.listdir(lSettingsPath) if os.path.isfile(os.path.join(lSettingsPath, f)) and f.split(".")[-1] == "py" and os.path.join(lSettingsPath, f) != __file__]
    import importlib.util
    for lModuleFilePathItem in lFileList:
        lModuleName = lModuleFilePathItem[0:-3]
        lFileFullPath = os.path.join(lSettingsPath, lModuleFilePathItem)
        lTechSpecification = importlib.util.spec_from_file_location(lModuleName, lFileFullPath)
        lTechModuleFromSpec = importlib.util.module_from_spec(lTechSpecification)
        lTechSpecificationModuleLoader = lTechSpecification.loader.exec_module(lTechModuleFromSpec)
        if lSubmoduleFunctionName in dir(lTechModuleFromSpec):
            #Run SettingUpdate function in submodule
            getattr(lTechModuleFromSpec, lSubmoduleFunctionName)(mDict)
    return mDict