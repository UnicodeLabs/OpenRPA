import sys
sys.path.append('../../')
import GUI
import keyboard
import subprocess
import time
#Highlight the UI Object in Folder explorer
GUI.UIOSelector_FocusHighlight([{"class_name":"CabinetWClass","backend":"uia"},{"ctrl_index":2},{"ctrl_index":0},{"ctrl_index":2},{"ctrl_index":0}])
#Wait 2 seconds
time.sleep(3)
#Loop: get child element of UI List
for lItem in GUI.UIOSelector_Get_UIO([{"class_name":"CabinetWClass","backend":"uia"},{"ctrl_index":2},{"ctrl_index":0},{"ctrl_index":2},{"ctrl_index":0}]).children():
    print(str(lItem))