def subprocess():
    import subprocess
    lCMD = "for /l %x in (1, 1, 5) do echo %x && ping 127.0.0.1 -n 2"
    lCMD = "git status"
    proc = subprocess.Popen(f'cmd /c {lCMD}', stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    # proc = subprocess.Popen('notepad', stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    #proc = subprocess.Popen('cmd /c git status', stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    #proc = subprocess.run(f'cmd /c {lCMD}', stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    print(123)
    #import pdb
    #pdb.set_trace()
    #tmp = proc.stdout.read()
    lListenBool = True
    while lListenBool:
        tmp = proc.stdout.readline()
        if tmp == b"":
            lListenBool = False
        #tmp = proc.stdout
        #print(tmp)
        print(tmp.decode("cp866"))
    print("Happy end")

def merge(a, b, path=None):
    "merges b into a"
    if path is None: path = []
    for key in b:
        if key in a:
            if isinstance(a[key], dict) and isinstance(b[key], dict):
                merge(a[key], b[key], path + [str(key)])
            elif a[key] == b[key]:
                pass # same leaf value
            else:
                raise Exception('Conflict at %s' % '.'.join(path + [str(key)]))
        else:
            a[key] = b[key]
    return a

# works
#print(merge({1:{"a":"A"},2:{"b":"B"}}, {2:{"c":"C"},3:{"d":"D"}}))
# has conflict
#merge({1:{"a":"A"},2:{"b":"B"}}, {1:{"a":"A"},2:{"u":"C"}})

lUACClientDictOld = {
    "pyOpenRPADict": {
        "CPKeyDict": {  # Empty dict - all access
            "yy":1,
            "Test":12
            # "CPKeyStr"{
            # }
        },
        "RDPKeyDict": {  # Empty dict - all access
            # "RDPKeyStr"{
            #   "FullscreenBool": True,
            #   "IgnoreBool":True,
            #   "ReconnectBool": True
            #   "NothingBool": True # USe option if you dont want to give some access to the RDP controls
            # }
        },
        "AgentKeyDict": {  # Empty dict - all access
            # "AgentKeyStr"{
            # }
        },
        "AdminDict": {  # Empty dict - all access
            "LogViewerBool": True,  # Show log viewer on the web page
            "CMDInputBool": True,  # Execute CMD on the server side and result to the logs
            "ScreenshotViewerBool": True,  # Show button to look screenshots
            "RestartOrchestratorBool": True,  # Restart orchestrator activity
            "RestartOrchestratorGITPullBool": True,  # Turn off (RDP remember) orc + git pull + Turn on (rdp remember)
            "RestartPCBool": True,  # Send CMD to restart pc
            "NothingBool": True  # USe option if you dont want to give some access to the RDP controls
        },
        "ActivityDict": {  # Empty dict - all access
            "ActivityListExecuteBool": True,  # Execute activity at the current thread
            "ActivityListAppendProcessorQueueBool": True  # Append activity to the processor queue
        }
    }

}

lUACClientDictNew = {
    "pyOpenRPADict": {
        "CPKeyDict": {  # Empty dict - all access
            "Test":True
        }
    }
}
print(merge(lUACClientDictOld, lUACClientDictNew))