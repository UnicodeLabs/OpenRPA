.. _module.robot.uiweb:

####################################
3. Функции UIWeb
####################################

************************
Общее
************************

Здесь представлено описание всех функций, необходимых для максимально эффективного управления web интерфейсами различных приложений.

**************************************************
Описание функций
**************************************************

Описание каждой функции начинается с обозначения L+,W+, что означает, что функция поддерживается в ОС Linux (L) и Windows (W)

.. automodule:: pyOpenRPA.Robot.UIWeb
    :members:
    :autosummary:

******************************
Быстрая навигация
******************************

- `Сообщество pyOpenRPA (telegram) <https://t.me/pyOpenRPA>`_
- `Сообщество pyOpenRPA (tenchat) <https://tenchat.ru/iMaslov?utm_source=19f2a84f-3268-437f-950c-d987ae42af24>`_
- `Сообщество pyOpenRPA (вконтакте) <https://vk.com/pyopenrpa>`_
- `Презентация pyOpenRPA <https://pyopenrpa.ru/Index/pyOpenRPA_product_service.pdf>`_
- `Портал pyOpenRPA <https://pyopenrpa.ru>`_
- `Репозиторий pyOpenRPA <https://gitlab.com/UnicodeLabs/OpenRPA>`_
