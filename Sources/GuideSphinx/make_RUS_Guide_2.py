import os, shutil, subprocess

# Build dir
gSourceDirAbsPathStr = os.path.abspath("")
gBuildDirAbsPathStr = os.path.abspath("..\\..\\Wiki\\RUS_Guide")
gBuildDirHTMLAbsPathStr = os.path.join(gBuildDirAbsPathStr, "html")
gBuildDirMDAbsPathStr = os.path.join(gBuildDirAbsPathStr, "markdown")

# list all dir which have img folder
# Copy to md and html

for root, subFolder, files in os.walk(os.path.abspath("")):
    for item in files:
        if item.endswith(".png") :

            fileNamePath = str(os.path.join(root,item))
            #print(fileNamePath)
            lFileItemBuildDirHTMLAbsPathStr = root.replace(gSourceDirAbsPathStr, gBuildDirHTMLAbsPathStr)
            lFileItemBuildHTMLAbsPathStr = str(os.path.join(lFileItemBuildDirHTMLAbsPathStr,item))
            if not os.path.exists(lFileItemBuildDirHTMLAbsPathStr):
                os.mkdir(lFileItemBuildDirHTMLAbsPathStr)
            shutil.copy2(fileNamePath, fileNamePath.replace(gSourceDirAbsPathStr, gBuildDirHTMLAbsPathStr))

            lFileItemBuildDirMDAbsPathStr = root.replace(gSourceDirAbsPathStr, gBuildDirMDAbsPathStr)
            lFileItemBuildMDAbsPathStr = str(os.path.join(lFileItemBuildDirMDAbsPathStr,item))
            if not os.path.exists(lFileItemBuildDirMDAbsPathStr):
                os.mkdir(lFileItemBuildDirMDAbsPathStr)
            shutil.copy2(fileNamePath, fileNamePath.replace(gSourceDirAbsPathStr, gBuildDirMDAbsPathStr))


# Run PDF generation
"""
gHTML2PDFPathList = [
    "index.html",
    "01_HowToInstall.html",
    "02_RoadMap.html",
    "03_Copyrights_Contacts.html"
]
gWKHTMLtoPDFPath = "..\\..\\Resources\\wkhtmltopdf\\bin\\wkhtmltopdf.exe"
gCMDRunStr = f'{os.path.abspath(gWKHTMLtoPDFPath)} --javascript-delay 5000 --load-error-handling ignore --enable-local-file-access '
gPDFOutputAbsPath = os.path.join(gBuildDirAbsPathStr, "pyOpenRPA_Guide_ENG.pdf")
for lItemStr in gHTML2PDFPathList:
    gCMDRunStr += f' {os.path.join(gBuildDirHTMLAbsPathStr, lItemStr)} '

gCMDRunStr += f' "{gPDFOutputAbsPath}" '
#os.system("cmd /c "+gCMDRunStr)
subprocess.call(gCMDRunStr.split(" "), shell=True)
print(gCMDRunStr)
"""