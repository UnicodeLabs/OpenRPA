cd %~dp0
@ECHO OFF
RD /S /Q "%~dp0..\..\Wiki\RUS_Guide\doctrees"
RD /S /Q "%~dp0..\..\Wiki\RUS_Guide\html"
RD /S /Q "%~dp0..\..\Wiki\RUS_Guide\markdown"

set PATH=%PATH%;%~dp0..\..\Resources\WPy64-3720\python-3.7.2.amd64
set PYTHONPATH=%PYTHONPATH%;%~dp0..\..\Resources\WPy64-3720\python-3.7.2.amd64
pushd %~dp0

REM Command file for Sphinx documentation

if "%SPHINXBUILD%" == "" (
	set SPHINXBUILD=python.exe -m sphinx
)
set SOURCEDIR=%~dp0
set BUILDDIR=..\..\Wiki\RUS_Guide

%SPHINXBUILD% >NUL 2>NUL
if errorlevel 9009 (
	echo.
	echo.The 'sphinx-build' command was not found. Make sure you have Sphinx
	echo.installed, then set the SPHINXBUILD environment variable to point
	echo.to the full path of the 'sphinx-build' executable. Alternatively you
	echo.may add the Sphinx directory to PATH.
	echo.
	echo.If you don't have Sphinx installed, grab it from
	echo.http://sphinx-doc.org/
	exit /b 1
)

%SPHINXBUILD% -M html %SOURCEDIR% %BUILDDIR% %SPHINXOPTS% %O%
%SPHINXBUILD% -M markdown %SOURCEDIR% %BUILDDIR% %SPHINXOPTS% %O%
"..\..\Resources\WPy64-3720\python-3.7.2.amd64\python.exe" make_RUS_Guide_2.py

..\..\Wiki\RUS_Guide\html\index.html
goto end

:help
%SPHINXBUILD% -M help %SOURCEDIR% %BUILDDIR% %SPHINXOPTS% %O%

:end
popd
pause>nul
