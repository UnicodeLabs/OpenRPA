cd %~dp0
@ECHO OFF
RD /S /Q "%~dp0..\..\Wiki\ENG_Guide\doctrees"
RD /S /Q "%~dp0..\..\Wiki\ENG_Guide\html"
RD /S /Q "%~dp0..\..\Wiki\ENG_Guide\markdown"

set PATH=%PATH%;%~dp0..\..\Resources\WPy64-3720\python-3.7.2.amd64\Scripts
set PYTHONPATH=%PYTHONPATH%;%~dp0..\..\Resources\WPy64-3720\python-3.7.2.amd64\Scripts
pushd %~dp0

REM Command file for Sphinx documentation

if "%SPHINXBUILD%" == "" (
	set SPHINXBUILD=sphinx-build
)
set SOURCEDIR=%~dp0
set BUILDDIR=..\..\Wiki\ENG_Guide

%SPHINXBUILD% >NUL 2>NUL
if errorlevel 9009 (
	echo.
	echo.The 'sphinx-build' command was not found. Make sure you have Sphinx
	echo.installed, then set the SPHINXBUILD environment variable to point
	echo.to the full path of the 'sphinx-build' executable. Alternatively you
	echo.may add the Sphinx directory to PATH.
	echo.
	echo.If you don't have Sphinx installed, grab it from
	echo.http://sphinx-doc.org/
	exit /b 1
)

%SPHINXBUILD% -M html %SOURCEDIR% %BUILDDIR% %SPHINXOPTS% %O%
%SPHINXBUILD% -M markdown %SOURCEDIR% %BUILDDIR% %SPHINXOPTS% %O%
"..\..\Resources\WPy64-3720\python-3.7.2.amd64\python.exe" make_ENG_Guide_2.py

..\..\Wiki\ENG_Guide\html\index.html
goto end

:help
%SPHINXBUILD% -M help %SOURCEDIR% %BUILDDIR% %SPHINXOPTS% %O%

:end
popd
pause>nul
