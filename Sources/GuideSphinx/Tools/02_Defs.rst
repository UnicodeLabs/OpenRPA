####################################
2. Функции StopSafe
####################################

How to use StopSafe on the robot side

.. code-block:: python

	from pyOpenRPA.Tools import StopSafe
	StopSafe.Init(inLogger=None)
	StopSafe.IsSafeStop() # True - WM_CLOSE SIGNAL has come. taskkill /im someprocess.exe


.. automodule:: pyOpenRPA.Tools.StopSafe
    :members:
    :autosummary:
	
	
.. automodule:: pyOpenRPA.Tools.Debugger
    :members:
    :autosummary:
