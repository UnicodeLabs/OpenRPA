import psutil
import datetime
import logging

#Studio settings
def Settings():
    import os
    mDict = {
        "Server": {
            "ListenPort_": "Порт, по которому можно подключиться к демону",
            "ListenPort": 8081,
            "ListenURLList": [
                {
                    "Description": "Local machine test",
                    "URL_": "Сетевое расположение сервера демона",
                    "URL": "http://127.0.0.1:8081"
                }
            ],
        },
        "Logger": logging.getLogger("Studio"),
        "Storage": {
            "Robot_R01_help": "Robot data storage in orchestrator env",
            "Robot_R01": {}
        },
        "ProcessBitness": { #Section for robot init
            "Python32FullPath": "..\\Resources\\WPy32-3720\\python-3.7.2\\python.exe", #Set from user: "..\\Resources\\WPy32-3720\\python-3.7.2\\OpenRPARobotGUIx32.exe"
            "Python64FullPath": "..\\Resources\\WPy64-3720\\python-3.7.2.amd64\\python.exe", #Set from user
            "Python32ProcessName": "OpenRPAUIDesktopX32.exe", #Config set once
            "Python64ProcessName": "OpenRPAUIDesktopX64.exe" #Config set once
        }
    }
    #Создать файл логирования
    # add filemode="w" to overwrite
    if not os.path.exists("Reports"):
        os.makedirs("Reports")
    ##########################
    #Подготовка логгера Robot
    #########################
    mRobotLogger=mDict["Logger"]
    mRobotLogger.setLevel(logging.INFO)
    # create the logging file handler
    mRobotLoggerFH = logging.FileHandler("Reports\ReportStudio_"+datetime.datetime.now().strftime("%Y_%m_%d")+".log")
    mRobotLoggerFormatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    mRobotLoggerFH.setFormatter(mRobotLoggerFormatter)
    # add handler to logger object
    mRobotLogger.addHandler(mRobotLoggerFH)
    ############################################
    return mDict