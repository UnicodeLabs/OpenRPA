#!C:\RPA\pyOpenRPA_repo\Resources\WPy64-3720\python-3.7.2.amd64\python.exe
# EASY-INSTALL-ENTRY-SCRIPT: 'Babel==2.9.0','console_scripts','pybabel'
__requires__ = 'Babel==2.9.0'
import re
import sys
from pkg_resources import load_entry_point

if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw?|\.exe)?$', '', sys.argv[0])
    sys.exit(
        load_entry_point('Babel==2.9.0', 'console_scripts', 'pybabel')()
    )
