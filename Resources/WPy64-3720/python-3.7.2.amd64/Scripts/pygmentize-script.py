#!C:\RPA\pyOpenRPA_repo\Resources\WPy64-3720\python-3.7.2.amd64\python.exe
# EASY-INSTALL-ENTRY-SCRIPT: 'Pygments==2.7.1','console_scripts','pygmentize'
__requires__ = 'Pygments==2.7.1'
import re
import sys
from pkg_resources import load_entry_point

if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw?|\.exe)?$', '', sys.argv[0])
    sys.exit(
        load_entry_point('Pygments==2.7.1', 'console_scripts', 'pygmentize')()
    )
